\chapter{Évolution et équilibre chimique (CPGE)}

\niveau{PSI}

    Nous nous limitons
    aux réactions chimiques uniques
    (une seule réaction à la fois)
    et qui se terminent par un équilibre
    (pas de réaction totale).

    \begin{manip}
        Solution de \ce{NaCl} saturée,
        ajout de \ce{HCl} concentré,
        précipite.
    \end{manip}
    Le précipité n'est pas lié
    au caractère acide.

    Dans cette leçon
    on s'intéresse
    aux équilibres chimiques,
    à l'évolution (au sens thermodynamique) des réactions
    et aux moyens dont l'on dispose,
    comme expérimentateur,
    pour favoriser
    ou empêcher
    des réactions.

  \section{Bases de thermochimie}

    \subsection{Potentiel thermodynamique}

    En thermochimie,
    on se place à
    $T_{ext} = \cst$
    et $P_{ext} = \cst$,
    et on suppose un équilibre
    thermique et mécanique
    entre le système et l'extérieur
    au début et à la fin
    de la réaction.
    Il convient alors d'utiliser
    l'enthalpie libre ($G = U - TS + PV$)
    comme potentiel thermodynamique,
    puisque pour une transformation finie,
    $\Delta G = - T S_c \leq 0$.

    \subsection{Potentiel chimique}

    Par ailleurs
    l'identité thermodynamique
    qui s'écrit~:
    \begin{equation*}
        \dd G
        = V \dd P
            - S \dd T
            + \sum_i(
                \underbrace{\eval{\pdv{G}{n_i}}_{P, T, n_{\strike{i}}}}_{\mu_i}
                \dd n_i)
    \end{equation*}
    nous renseigne sur
    l'enthalpie libre molaire partielle $\mu_i$
    associée à chaque constituant du système.

    Dans le cadre du programme de PSI,
    le potentiel chimique $\mu_i$ d'un constituant
    s'écrit selon l'expression générale~:
    \begin{equation*}
        \mu_i(T, P, n_j) = \mu_i^\std(T, P) + RT \ln(a_i)
    \end{equation*}
    avec $a_i$ l'activité du constituant,
    et $\mu_i^\std$ son potentiel chimique standard
    tabulé.

    \subsection{Enthalpie libre standard de réaction}

    Comme lors d'une réaction chimique
    l'avancement infinitésimal $\dd \xi$
    permet d'écrire simplement
    la variation de quantité de matière
    de chaque constituant~:
    $\dd n_i = \nu_i \dd \xi$~;
    alors
    l'identité thermodynamique se réécrit~:
    \begin{equation*}
        \dd G
        = V \dd P
            - S \dd T
            + \underbrace{\sum_i(\mu_i \nu_i)}_{\Delta\mathrm{r}G} \dd \xi
    \end{equation*}
    En somme,
    pour une réaction chimique
    monobare
    et monotherme,
    le bilan d'enthalpie libre s'écrit~:
    $\Delta G = \Delta\mathrm{r}G \cdot \dd \xi = - T S_c \leq 0$,
    avec $\Delta\mathrm{r}G = \sum_i(\mu_i \nu_i)$.

    \subsection{Critères d'évolution et d'équilibre}

    Puisque $\Delta\mathrm{r}G \cdot \dd \xi \leq 0$,
    on va pouvoir déduire du signe de $\Delta\mathrm{r}G$,
    le signe de $\dd \xi$,
    et donc le sens d'avancement de la réaction~:
    \begin{itemize}
        \item si $\Delta\mathrm{r}G < 0$ alors $\dd \xi > 0$,
            la réaction se fait dans le sens direct,
        \item si $\Delta\mathrm{r}G > 0$ alors $\dd \xi < 0$,
            la réaction se fait dans le sens indirect,
        \item si $\Delta\mathrm{r}G = 0$,
            la réaction est à l'équilibre.
    \end{itemize}

    \subsection{Constante d'équilibre}

    Avec~:
    \begin{equation*}
        \Delta\mathrm{r}G
        = \sum_i(\mu_i \nu_i)
        = \sum_i(\mu_i^\std \nu_i) + RT \sum_i(\ln(a_{i}^{\nu_i}))
        = \Delta\mathrm{r}G^\std + RT \ln(Q_r)
    \end{equation*}
    où $Q_r$ est le quotien réactionnel,
    et $\Delta\mathrm{r}G^\std$ une constante~;
    alors
    à l'équilibre,
    \begin{equation*}
        \Delta\mathrm{r}G
        = 0
        \implies
        RT \ln(Q_{r,eq})
        = - \Delta\mathrm{r}G^\std
    \end{equation*}
    on définit~:
    $K^\std(T) \defeq \exp(-\frac{\Delta\mathrm{r}G^\std}{RT})$
    de sorte que~:
    $Q_r = K^\std(T)$
    à l'équlibre.

    Finalement,
    $\Delta\mathrm{r}G = RT \ln\qty(\frac{Q_r}{K^\std(T)})$
    et l'on retrouve des notions de première année~:
    \begin{itemize}
        \item si $Q_r < K^\std(T)$ alors
            la réaction se fait dans le sens direct,
        \item si $Q_r > K^\std(T)$ alors
            la réaction se fait dans le sens indirect,
        \item si $Q_r = K^\std(T)$ alors
            la réaction est à l'équilibre.
    \end{itemize}

    On schématise ce comportement
    avec un axe horizontal en $Q_r$,
    sur lequel on place $K^\std$.
    \begin{todo}
        Faire le schéma
        en illustrant la manip d'introduction.
    \end{todo}

  \section{Thermochimie des procédés chimiques}

    Dans cette partie
    nous exploitons les connaissances introduites
    concernant les conditions d'évolution et d'équilibre
    d'un système chimique.
    L'idée est de forcer des variations
    de $Q_r$
    ou de $K^\std(T)$
    pour favoriser ou empêcher
    la formation de produits.

    \subsection{Modification de $Q_r$ par ajout ou extraction d'une espèce}

    \subsubsection{Synthèse de l'acétate linalyle, à l'odeur de lavande
        \cite{ficheux}}

    \begin{manip}
        Synthèse de l'acétate de linalyle
        à partir de linalol
        et d'acide éthanoïque,
        dans le cyclohexane
        sur \propername{Dean-Stark}
        pour déplacer l'équilibre
        vers les produits.
    \end{manip}

    Le principe est le même
    que dans le cas précédent.
    Avant on forcait la réaction
    dans le sens indirect
    en ajoutant un produit,
    maintenant on la force
    dans le sens direct
    en retirant un produit.

    \subsection{La température et la pression \cite{bup879}}

    \begin{manip}
        Équilibre \ce{NO2(g) <--> N2O4(g)}
        sous pressions
        ou à températures
        variable.
        On synthétise \ce{NO2}
        par un protocol similaire à
        \cite{bup879}
        en veillant à être
        plus soigné
        autant sur la sécurité
        que sur le matériel choisit.
    \end{manip}

    Nous allons étudier le cas
    du dioxyde d'azote
    qui peut réagir selon
    \ce{2 NO2(g) <--> N2O4(g)}~;
    cette réaction a
    une enthalpie standrad de réaction
    $\Delta\mathrm{r}H^\std = \SI{-57.23}{\kilo\joule\per\mol}$,
    elle est donc exothermique.
    D'autre part
    on constate que
    lorsqu'elle se fait
    dans le sens direct,
    la quantité de matière
    est divisée par deux.

    \subsubsection{Pression ($Q_r$)}

    Le quotien de réaction
    s'écrit~:
    \begin{equation*}
        Q_r
        = \frac{a_{\ce{N2O4}}}{a_{\ce{NO2}}^2}
        = \frac{P_{\ce{N2O4}}}{P_{\ce{NO2}}^2} \cdot P^\std
        = \frac{x_{\ce{N2O4}}}{x_{\ce{NO2}}^2} \cdot \frac{P^\std}{P_{tot}}
    \end{equation*}
    On constate qu'il dépend de la pression,
    on dira qu'elle est \emph{facteur d'équilibre}.

    Une augmentation de $P_{tot}$
    fait diminuer $Q_r$~:
    la réaction se fait dans le sens direct.

    \subsubsection{Température ($K^\std(T)$)}

    L'expression mathématique
    du phénomène
    nécessite de revenir à du calcul formel,
    on a~:
    \begin{equation*}
        \overbrace{
        \dd G
        = V \dd P
            - S \dd T
            + \Delta\mathrm{r}G \dd \xi
        }^{\qq{identité thermodynamique}}
        \implies -S = \eval{\pdv{G}{T}}_P
    \end{equation*}
    d'où~:
    \begin{equation*}
        G
        \defeq H - TS
        = H + T \eval{\pdv{G}{T}}_P
    \end{equation*}
    on pourra en déduire
    la relation de \propername{Gibbs-Helmoltz}~:
    \begin{equation*}
        \frac{H}{T^2}
        = \frac{G}{T^2} - T \eval{\pdv{G}{T}}_P
        = - \eval{\pdv{\frac{G}{T}}{T}}_P
    \end{equation*}
    Cette relation
    ici écrite sur $G$
    peut s'appliquer à une réaction
    finie monobare
    et donner~:
    \begin{equation*}
        \frac{\Delta_\mathrm{r} H}{T^2}
        = - \eval{\pdv{\frac{\Delta_\mathrm{r} G}{T}}{T}}_P
    \end{equation*}
    alors
    dans l'état standard~:
    \begin{equation*}
        \frac{\Delta_\mathrm{r} H^\std}{T^2}
        = - \eval{\pdv{T}(\frac{- R T \ln(K^\std(T))}{T})}_P
        \implies
        \frac{\Delta_\mathrm{r} H^\std}{R T^2}
        = \dv{\ln(K^\std(T))}{T}
    \end{equation*}
    la relation de \propername{Van't Hoff}.

    Pour $\Delta_\mathrm{r} H^\std < 0$,
    la dérivée $\dv{T}(\ln(K^\std(T)))$ est négative,
    donc $K^\std(T)$ est
    une fonction décroissante
    de $T$.
    Autrement dit,
    l'augmentation de la température
    déplace la constante d'équilibre
    vers une plus petite valeure~:
    lors un équilibre établi à $T_2$
    suivi d'une diminution de température jusqu'à $T_1 < T_2$,
    la réaction rejoint le nouvel équilibre
    en avancant dans le sens indirect.

    \subsection{Principe de \propername{Le Châtelier}}

\plusloin{
    Suite à cette leçon
    nous pourrons étudier des processus de
    réactions chimiques en chaînes.
    Nous verrons que des combinaisons linéaires
    d'équations chimiques
    conduisent à des relations simples
    sur les grandeurs thermodynamiques
    introduites dans cette leçon.

    Nous pourrons aussi
    apprendre à déterminer les valeurs
    des grandeurs standard de réaction
    en utilisant des tables de donées.
}
