\chapter{Biréfringence, pouvoir rotatoire}

\section{Biréfringence naturelle~: rayons ordinaire et extraordinaire}

    \subsection{Mise en évidence des deux rayons}

    On utilise une lumière blanche
    et un diaphragme
    pour éclairer
    un petit morceau
    de cristal de spath
    taillé selon son axe optique.

    \paragraph{Résultat attendu}
    Sur un écran
    on observe la présence
    de deux tâches lumineuses~:
    l'une d'entre elle
    tourne autour de l'autre
    lorsque l'on pivote le cristal.
    La tâche fixe
    est issue du rayon \emph{ordinaire}
    et l'autre
    du rayon \emph{extraordinaire}.

    \subsection{Polarisation des rayons}

    \paragraph{Résultat attendu}
    Les deux faiseaux
    sont polarisés perpendiculairement
    l'un à l'autre.

    \paragraph{Mesures}
    En plaçant
    un polariseur
    en amont du spath
    (pour avoir une lumière polarisée récilignement)
    et un autre
    en aval (comme analyseur),
    on peut chercher à éteindre
    l'une et l'autre des tâches
    en pivotant l'analyseur.

    \paragraph{Rappels}

    Dans la matière
    le vecteur déplacement électrique $\vb{D}$
    s'écrit~:
    \begin{equation*}
        \vb{D}
        = \epsilon_0 \vb{E} + \vb{P}
    \end{equation*}
    et le vecteur polarisation
    est~:
    \begin{equation*}
        \vb{P}
        = \epsilon_0 [\chi] \vb{E}
        \qq{d'où}
        \vb{D}
        = \epsilon_0 [\epsilon_r] \vb{E}
    \end{equation*}

    Si la matrice $[\chi]$
    est diagonale,
    le milieu est isotrope
    et les vecteurs $\vb{D}$ et $\vb{E}$
    sont parallèles.
    Si elle s'écrit comme~:
    \begin{equation*}
        [\chi]
        = \mqty[\dmat{\chi, \chi, \chi_z}]
    \end{equation*}
    le milieu est dit \emph{uniaxe}
    d'axe optique $\vu{z}$
    et les vecteurs $\vb{D}$ et $\vb{E}$
    ne sont pas collinéaires.
    Dans ce cas,
    tout se passe comme si
    le milieu avait deux indices optiques
    $n_o$ et $n_e$,
    le vecteur $\vb{D}$
    se décompose comme
    $\vb{D} = \vb{D}_o + \vb{D}_e$.
    $\vb{D}_O$ est collinéaire à $\vb{E}$
    alors que $\vb{D}_e$ ne l'est pas.

    Pour la composante
    ou le vecteur déplacement
    est collinéaire au champ électrique
    le vecteur d'onde $\vb{k}$
    est collinéaire
    au vecteur de \propername{Poynting} $\vb{\Pi}$~:
    la phase
    et l'énergie
    se propagent dans la même direction.
    On rapelle la définition~:
    \begin{equation*}
        \vb{\Pi}
        \defeq \vb{E} \crossproduct \vb{H}
    \end{equation*}

\section{Spectre cannelé des lames de quartz}

    On éclaire en lumière blanche
    parallèle,
    polarisée (via un polariseur + filtre AC),
    avec diaphragme et une lentille,
    une lame de quartz
    d'épaisseur $e$
    taillée parallèlement à son axe optique.

    Un second polariseur
    croisé au premier
    analyse la lumière sortante du quartz
    et une lentille
    fait converger la lumière
    vers un spectrophotomètre USB.

    \paragraph{Résultat attendu}
    On obtient un spectre cannelé
    où la lumière est éteinte
    pour des longueurs d'ondes espacées régulièrement.
    L'écart entre ces longueurs d'ondes
    permet de remonter
    à l'écart entre les deux indices du quartz~:
    ${\Delta n}^{\mathrm{tab}} = \num{9.5e-3}$.

    \paragraph{Mesures}
    Pour deux longueurs d'ondes éteintes
    $\lambda_{p'}$ et $\lambda_p$,
    on peut montrer que~:
    \begin{equation*}
        \Delta n
        = \frac{\lambda_{p'} \lambda_p}{e (\lambda_{p'} - \lambda_p)} \Delta p
    \end{equation*}

    \paragraph{Remarques}
    Les cannelures
    sont résultat d'interférences
    entre la lumière de chaque rayon.
    L'analyseur
    permet de maximiser le contraste~:
    pour qu'il y ait interférences
    on doit projeter les deux faisceaux
    sur le même axe de polarisation.
    Dans cette expérience
    il n'y a pas séparation géométrique des faisceaux.

\section{Pouvoir rotatoire naturel}

    On éclaire
    avec un laser
    une série de lames de quartz
    d'épaisseur $e$ variable
    taillées perpendiculairement à leurs axes optiques.
    Un analyseur
    permet d'éteindre
    la lumière projetée sur un écran.

    \paragraph{Résultat attendu}
    La relation
    $\alpha = \fof(e)$
    est linéaire
    on doit trouver
    $\frac{\alpha}{e} = \SI{18.9}{\deg\per\milli\metre}$.

    \paragraph{Mesures}
    On prendra
    comme référence pour l'angle $\alpha$
    l'angle de polarisation du laser,
    que l'on détermine en l'abscence de quartz.

    \paragraph{Remarques}
    $\frac{\alpha}{e}$
    dépend de $\lambda$
    d'où le besoin
    d'utiliser un laser.
    Cette technique
    est utilisée en chimie
    pour caractériser des solutions.

\section{Pouvoir rotatoire unduit~: effet \propername{Faraday}}

    Un morceau de verre flint
    est placé dans les lignes de champ
    d'un électroaimant.
    Les lignes de champ magnétique
    sont collinéaires
    au banc optique.

    La costante $V = \frac{\alpha}{B e}$
    est caractéristique du matériaux.
    $V = \SI{5}{\deg\per\milli\metre\per\tesla}$.

    \paragraph{Mesures}
    En l'abscende de champ,
    on éteint la lumière en croisant les polariseurs.
    Puis en produisant le champ,
    on observe un rallumage de la tâche.
    On relève le delta d'angle
    pour lequel la tâche s'éteint à nouveau.
