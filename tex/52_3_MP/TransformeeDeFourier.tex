\chapter{Transformée de \propername{Fourier}}

On rapelle
la définition de
la transformée de \propername{Fourier}~:
\begin{equation*}
    \tf[f]{s}
    \defeq \int_{-\infty}^{+\infty} s(t) e^{-2 i \pi f t} \dd t
\end{equation*}

On utilisera
le produit de convolution,
noté $\conv$
et défini par~:
\begin{equation*}
    (s \conv f)(t)
    = \int_{-\infty}^{+\infty} s(\tau) f(t - \tau) \dd \tau
\end{equation*}

Et on exploitera
la relation
entre tansformée de \propername{Fourier}
et produit de convolution~:
\begin{equation*}
    \tf{s \cdot f}
    = \tf{s} \conv \tf{f}
\end{equation*}

\section{Acquisition numérique d'un signal}

    \subsection{Échantillonage}

    \begin{figure}[H]
        \centering
        \begin{tikzpicture}
            [declare function = {
                fmin = 5 ;
                fmax = 15 ;
                w = fmax - fmin ;
                spectra(\x)
                    = ( (fmin < \x) * (\x < fmax) )
                        * (fmax - \x) / w
                    + ( (- fmin > \x) * (\x > - fmax) )
                        * (fmax + \x) / w;
            }]
            \begin{axis}[
                height = 5 cm,
                width = \textwidth,
                ylabel = {$\tf{s}$},
                xlabel = {$f (\si{\hertz})$},
                xmin = -40 ,
                xmax = 200 ,
                domain = -40:200,
                samples=500,
                ymin = 0,
            ]
            \addplot[mark=none] { spectra(x) } ;
            \end{axis}
        \end{tikzpicture}
    \end{figure}

    L'échantillonage
    est modélisé
    par la fonction~:
    \begin{equation*}
        e(t)
        = \sum_{n=-\infty}^{+\infty} \delta(t - n T_e)
        \defeq \comb{T_e}(t)
        \qq{avec}
        \tf[f]{e}
        = \comb{f_e}(f)
    \end{equation*}

    Cette fonction
    est multipliée au signal $s(t)$
    lors de l'opération~:
    \begin{equation*}
        s(t) \cdot e(t)
        = s(t) \comb{T_e}(t)
    \end{equation*}

    La transformée de \propername{Fourier}
    du signal échantilloné
    est~:
    \begin{equation*}
        \tf{s \cdot e}
        = \tf{s} \conv \tf{\comb{T_e}}
        = \tf{s} \conv \comb{f_e}
    \end{equation*}
    Ce qui correspond
    (étant donné
    la convolution par un $\delta$ de \propername{Dirac})
    à une répétition
    du spectre de $s(t)$,
    tout les $f_e$.

    \begin{figure}[H]
        \centering
        \begin{tikzpicture}
            [declare function = {
                fmin = 5 ;
                fmax = 15 ;
                w = fmax - fmin ;
                spectra(\x)
                    = ( (fmin < \x) * (\x < fmax) )
                        * (fmax - \x) / w
                    + ( (- fmin > \x) * (\x > - fmax) )
                        * (fmax + \x) / w;
            }]
            \begin{axis}[
                height = 5 cm,
                width = \textwidth,
                ylabel = {$\tf{s}$},
                xlabel = {$f (\si{\hertz})$},
                extra x ticks={80,160},
                extra x tick labels={$f_e$, $2 f_e$},
                extra x tick style={ticklabel pos=top},
                xmin = -40 ,
                xmax = 200 ,
                domain = -40:200,
                samples=500,
                ymin = 0,
                legend entries={$f_e = \SI{80}{\hertz}$}
            ]
            \addplot[mark=none] { spectra(x-00) } ;
            \addplot[mark=none] { spectra(x-80) } ;
            \addplot[mark=none] { spectra(x-160) } ;
            \end{axis}
        \end{tikzpicture}
    \end{figure}

    \subsection{Fenêtre d'acquisition}

    \begin{figure}[H]
        \centering
        \begin{tikzpicture}
            \begin{axis}[
                height = 5 cm,
                width = \textwidth,
                ylabel = {$\tf{s}$},
                xlabel = {$f (\si{\hertz})$},
                xmin = -30 ,
                xmax = +30 ,
                domain = -30:30,
                samples=500,
                ymin = 0,
            ]
            \addplot[mark=none] coordinates { (-10, 0) (-10, 1) } ;
            \addplot[mark=none] coordinates { (10, 0) (10, 1) } ;
            \end{axis}
        \end{tikzpicture}
    \end{figure}

    L'acquisition,
    sur une fenêtre de temps
    de durée $T_a$,
    est modélisée par la fonction~:
    \begin{equation*}
        a(t)
        = \Pi_{T_a}(t)
        = \left\{\begin{array}{rl}
                1 &\qq{si} 0 \leq x \leq T_a \\
                0 &\qq{sinon}
            \end{array}\right.
        \qq{avec}
        \tf[f]{a}
        = \sin_c(f)
    \end{equation*}

    On a donc,
    lors de l'acquisiton~:
    \begin{equation*}
        s(t) \cdot a(t)
        = s(t) \Pi_{T_a}(t)
    \end{equation*}
    Et on s'intéresse
    à la transformée de \propername{Fourier}~:
    \begin{equation*}
        \tf[f]{s \cdot a}
        = \tf[f]{s} \conv \tf[f]{a}
        = \tf[f]{s} \conv T_a \sin_c(2 \pi f \flatfrac{T_a}{2})
    \end{equation*}

    \begin{figure}[H]
        \centering
        \begin{tikzpicture}
            \begin{axis}[
                height = 5 cm,
                width = \textwidth,
                ylabel = {$\tf{s}$},
                xlabel = {$f (\si{\hertz})$},
                extra x ticks={-9.5,10.5},
                extra x tick labels={$-10+\frac{1}{T_a}$,$10+\frac{1}{T_a}$},
                extra x tick style={ticklabel pos=top},
                xmin = -30 ,
                xmax = +30 ,
                domain = -30:30,
                samples=500,
                ymin = 0,
                legend entries={$T_a = \SI{2}{\second}$}
            ]
            \addplot[mark=none] {abs(
                    2 * 1/(3.14 * (x+10) * 2) * (
                        sin(deg(3.14 * (x+10) * 2)) )
                    + 2 * 1/(3.14 * (x-10) * 2) * (
                        sin(deg(3.14 * (x-10) * 2)) )
                )} ;
            \end{axis}
        \end{tikzpicture}
    \end{figure}

    \subsection{Échantillonage sur une fenêtre d'acquisition}

    Le nombre de points $N$
    va prendre de l'importance
    et s'écrire~:
    $N = \frac{T_a}{T_e} = T_a f_e$.

\section{Largeur des raies spectrales}

    \subsection{Diapason}

    \subsection{Raie d'une lampe à vapeur}

    \paragraph{Origine microscopique}
    Effet \propername{Doppler} (gausienne)
    et collisions (lorentzienne).

\section{Lentilles, optique de \propername{Fourier}}
