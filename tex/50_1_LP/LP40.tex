\chapter{Confinement d'une particule et quantification de l'énergie}

\niveau{L2}

\prereqstart
{Mécanique classique}
{Notion de fonction d'onde}
{Équation de \propername{Schrödinger}}
\prereqstop

\section{Approche historique}

    \subsection{Les spectres discrets de raies \cite{dufour}}

    Les premières raies spectrales
    sont observées avec des prismes
    par
    \propername[Thomas]{Melvill}
    (1752, Sodium, émission),
    \propername[William]{Wollaston}
    (1802, Soleil, absorbtion),
    \propername[Joseph]{Fraunhofer}
    (1814, Sodium dans le Soleil, absorbtion),
    et
    \propername{Angström}
    (hydrogène).

    En 1885, \propername{Balmer}
    puis en 1889, \propername{Rydberg}
    donnent
    une structure mathématique régulière
    en longueur d'ondes
    aux raies visibles de l'hydrogène~:
    \begin{equation*}
        \frac{1}{\lambda_n}
        = \glssymbol{Ry} \qty(\frac{1}{2^2} - \frac{1}{n^2})
    \end{equation*}
    (avec la constante alors expérimentale $\glsfull{Ry}$).
    Ils utilisent cette formule
    pour prédire avec succès
    la longueur d'onde d'autre raies de l'hydrogène
    situées dans l'ultraviolet.
    \begin{todo}
        Ajouter un spectre annoté de l'hydrogène.
    \end{todo}

    Suite aux travaux de \propername{Planck}
    sur le corps noir (1900),
    la présences de ces raies
    pose un problème à résoudre.
    Mais l'effet photoélectrique
    d'\propername{Einstein} (et \propername{Planck})
    en explique une partie.

    \subsection{Modèle de \propername{Bohr} de l'atome}

    \begin{todo}
        Parler
        du problème du non rayonnement des atomes,
        de leur stabilité,
        et de la désionisation
        qui donne toujours les mêmes atomes.
    \end{todo}

    Il s'agit d'un traitement classique de l'atome,
    dans lequel on introduit
    une hypothèse de quantification.

    On considère l'atome d'hydrogène comme
    un noyau de masse $m_p$
    autour duquel circule un électron de masse $m \ll m_p$.
    On se place dans les hypothèses
    du problème à deux corps
    type mouvement de \propername{Képler},
    avec une force d'interaction électrostatique~:
    \begin{equation*}
        m \vb{a}
        = \frac{- q^2}{4 \pi \epsilon_0 r^2} \vu{r}
        = \frac{e^2}{r^2} \vu{r}
        \qq{avec}
        e^2
        \defeq \frac{q^2}{4 \pi \epsilon_0}
    \end{equation*}
    Dans le cadre d'une trajectoire circulaire,
    on écrira~:
    \begin{equation*}
        m r \dt{\theta}^2
        = \frac{e^2}{r^2}
        \qq{et}
        \norm{\vb{L}}
        = m r^2 \dt{\theta}
    \end{equation*}

    Pour résoudre les problèmes énnoncés,
    \propername[Niels]{Bohr} introduit en 1913
    un postulat selon lequel
    il existe des orbites circulaires stables pour les électrons~:
    une fois sur l'une de ces orbites,
    l'électron de rayonne aucune énergie électromagnétique.
    Il ajoute que
    l'électron peut passer d'une orbite stable
    à une autre
    par absorption ou émission
    d'un photon dont l'énergie correspond
    à l'écart en énergie de l'électron
    entre les deux orbites.
    \begin{todo}
        Schéma
    \end{todo}
    La \emph{règle de quantification}
    que donne \propername{Bohr}
    pour identifier les orbites stables
    parmis les autres
    s'écrit~:
    \begin{equation*}
        \norm{\vb{L}}
        = n \hbar
    \end{equation*}
    En conséquences,
    les expressions de $\vb{a}$ et $\vb{L}$
    permettent d'écrire~:
    \begin{equation*}
        \frac{n^2 \hbar^2}{m r^3}
        = \frac{e^2}{r^2}
        \implies
        r
        = \frac{n^2 \hbar^2}{m e^2}
        \defeq a_0 n^2
    \end{equation*}
    avec $a_0$ le rayon de \propername{Bohr}.

    L'énergie potentielle (d'origine électrostatique) de l'électron
    sur l'une de ces orbites
    s'écrit~:
    \begin{equation*}
        E_p
        = - \frac{e^2}{r_n}
    \end{equation*}
    le principe fondamental de la dynamique~:
    \begin{align*}
        m \vb{a}
        &= - \pdv{E_p}{r_n} \vu{r}
        = - \frac{e^2}{r_n^2} \vu{r} \\
        &= - m \frac{v^2}{r_n} \vu{r}
    \end{align*}
    donne l'énergie cinétique~:
    \begin{equation*}
        E_c
        = \frac{m v^2}{2}
        = \frac{e^2}{2 r_n}
    \end{equation*}
    d'où l'énergie mécanique du système~:
    \begin{equation*}
        E_n
        = \frac{-1}{2} \frac{e^2}{r_n}
        = \frac{-1}{2} \frac{e^2}{a_0 n^2}
    \end{equation*}

    L'hypothèse de \propername{Bohr}
    sur la position des électrons
    implique une quantification de leur énergie.
    Comme le suggère le titre de la leçon
    il existe un lien fort entre
    confinement (ici sur une orbite)
    et quantification de l'énergie.

    On retouve
    l'expression expérimentale de \propername{Ry}
    pour la longueur d'onde d'une raie de l'hydrogène
    correspondant à une transition de l'électron
    d'une orbite $i$ vers $j$,
    avec
    $\Delta E = \frac{h c}{\lambda}$~:
    \begin{equation*}
        \Delta E
        = E_j - E_i
        = \frac{-e^2}{2} \qty(\frac{1}{r_j} - \frac{1}{r_i})
    \end{equation*}
    en réintroduisant toutes les grandeurs~:
    \begin{equation*}
        \frac{1}{\lambda}
        = \frac{-m q^4}{8 \epsilon_0^2 h^3 c}
            \qty(\frac{1}{j^2} - \frac{1}{i^2})
        \defeq \glssymbol{Ry} \qty(\frac{1}{j^2} - \frac{1}{i^2})
    \end{equation*}
    où l'on donne une expression
    pour $\glssymbol{Ry}$.
    On remarquera que
    le calcul permet
    de déterminer l'énergie d'ionisation de l'hydrogène
    en considérant
    une transition de l'état $i = 1$
    vers $j = \infty$~:
    $E_{ion}
    = h c \glssymbol{Ry}
    = \SI{13.6}{\eV}$.

\section{Confinement d'une particule}

    \subsection{Puit de potentiel infini \cite{dufour}}

    \subsubsection{Équation de \propername{Schrödinger} indépendante du temps
        \cite{dufour}}

    \begin{draft}
        Donner l'équation de \propername{Schrödinger},
        présenter la séparation des variables,
        la solution sur $\xi(t)$
        puis l'équation sur $\phi(x)$.
    \end{draft}

    \subsubsection{Résolution}

    \begin{draft}
        Présentation,
        résolution.
        Passer vite sur les cas sans intéret.
        Spectre et fonctions d'onde \cite{dufour}.
        Même solutions que la corde vibrante.
        État de plus basse énergie on nulle~: Heisenberg.
    \end{draft}
    \begin{pydo}
        Ajouter une animation
        qui trace les
        $\Psi_n(x, t)$
        et
        $\abs{\Psi_n(x, t)}^2$.
    \end{pydo}

    \subsubsection{Applications numériques}

    Le puit étudié ici
    est très théorique
    mais certains problèmes réels
    s'en approchent~:
    \begin{itemize}
        \item nucléons dans le noyau,
        \item électrons dans des jonctions de semi-conducteurs,
        \item particule $\alpha$ dans un noyeau de $\ce{U}$.
    \end{itemize}

    \subsection{Retour sur l'atome d'hydrogène}

    \begin{draft}
        Rapidement,
        donner le potentiel (non nul mais indépendant du temps),
        dire que l'on peut séparer les variables
        temporelles et spaciales
        puis radiales et angulaires,
        dire que
        les calculs sont très longs
        pour $Y(\theta, \phi)$,
        et donner l'équation pour $R(r)$.

        On en déduit les énergies,
        qui grandissent en $\flatfrac{1}{n^2}$
        et se resserent.

        L'existence d'une énergie minimale
        non nulle
        donne un rayon minimal
        non nul
        qui assure la stabilité de la matière
        et des solutions aux autres problèmes
        que \propername{Bohr}
        cherchait à résoudre.
    \end{draft}
    \begin{pydo}
        Préparer des graph
        des solutions radiales
        et de la densité de probabilité
        pour $l=0$.
    \end{pydo}
    \begin{draft}
        On fera remquer que
        la solution diverge en $r = 0$
        mais ça ne pose pas de problème
        quant à la densité de probabilité
        car en coordonnées sphériques,
        elle s'écrit~:
        \begin{equation*}
            \rho(r, \theta, \phi)
            = \abs{\Psi(r, \theta, \phi)}^2
                r^2 \dd r \sin \theta \dd \theta \dd \phi
        \end{equation*}
        et~:
        $r^2 e^{-\frac{2r}{a_0}}$
        ne diverge pas.
    \end{draft}

\plusloin{
    Dans cette leçon
    nous avons étudié en détails
    le problème dit
    de la \emph{particule dans une boîte}.
    Il s'agit bien d'une particule
    confinée
    et l'on trouve
    que son énergie est quantifiée.
    Nous avons vu
    comment le procédé de ce calcul
    peut être appliqué pour résoudre
    le problème de l'atome d'hydrogène.

    Dans la suite du cours,
    on pourra illuster
    la résolution de l'équation de \propername{Schrödinger}
    sur le problème du potentiel harmonique,
    qui modélise de manière approchée
    les potentiels continus
    autour des positions d'équilibres.
}
