\chapter{Rayonnement dipolaire électrique}

\niveau{L2}

\prereqstart
{Électromagnétisme ($\vb{E}$, $\vb{B}$, $V$, $\vb{A}$)}
{Vecteur de \propername{Poynting}}
{Dipôle électrostatique}
{Électron élastiquement lié}
\prereqstop

Dans les leçons précédentes,
nous avons étudié
l'interaction entre la lumière et les milieux~:
dans une description mésoscopique,
nous expliquions comment
le champ électromagnétique
se voyait modifié en présence de matière.
Notre description mettait en jeu
des charges
dites
liées ou libres,
qui
par leur mouvement,
étaient la source d'une réponse
aux ondes
de la part du matériaux.
L'un des phénomènes
passé sous silence,
est le suivant~:
une charge en mouvement
génère autour d'elle
un champ électrique
et un champ magnétique,
elle doit rayonner.

Dans cette leçon,
nous n'étudions pas
l'aborbstion et la dispersion
des ondes
dans les milieux,
mais
le rayonnement émis
par un milieux soumis
à une excitation électromagnétique.
Nous travaillerons
sur un modèle simple~:
le dipôle électrique oscillant.

\section{Champs $\vb{E}$ et $\vb{B}$ d'un dipôle électrique oscillant
    \cite{perezelectromag, feynmanem1}}

    \subsection{Modèle et approximation dipolaire}

    \begin{figure}[H]
        \centering
        \begin{tikzpicture}
            \coordinate (O) at (0, 0);
            \coordinate (P) at (0, +.5);
            \coordinate (M) at (9, 2);
            \draw (O) node[anchor=east]{$O(-q)$};
            \draw (P) node[anchor=east]{$P(+q)$};
            \draw (M) node[anchor=west]{$M$};

            \draw[thick,->] (O) -- node[anchor=west]{$\vb{p}$} (P);
            \draw[color=gray,->] (O) -- node[below]{$\vb{r}$} (M);
            \draw[dotted,color=gray,->] (P) -- node[above]{$\vb{r^+}$} (M);
        \end{tikzpicture}
    \end{figure}

    On considère
    deux charges
    $+q$ et $-q$
    situées en
    $P$ et $O$
    (avec $\norm*{\va{OP}} = d$).
    Ces charges
    constituent
    un dipôle
    de moment dipolaire
    $\vb{p} = q \va{OP}$.

    On se place
    dans un système de coordonnées
    sphérique usuel
    de sorte que l'axe porté par $\vu{z}$
    soit aligné avec $\va{OP}$.
    D'où~:
    $\vb{p} = q d \vu{z}$.

    On étudie la situation
    en un point $M$ de l'espace
    décrit par le rayon vecteur $\vb{r}$.
    On définit le vecteur~:
    \begin{equation*}
        \vb{r^+} = \va{PM}
    \end{equation*}
    et l'angle~:
    \begin{equation*}
        \theta = (\vb{p}, \vb{r})
    \end{equation*}
    Dans le cadre de
    l'\emph{approximation dipolaire}
    nous allons considérer
    $d \ll r$,
    ce qui revient à
    observer le dipôle
    depuis une très grande distance.
    Nous avons,
    par géométrie~:
    \begin{equation*}
        \vb{r^+}
        = \vb{r} - d\vu{z}
        \implies
        \vb{r^+}^2
        = r^2 + d^2 - 2rd\cos(\theta)
    \end{equation*}
    Alors,
    \begin{equation*}
        r^+
        \approx r - d\cos(\theta)
    \end{equation*}

    \subsection{Potentiels}

    \subsubsection{Potentiel vecteur $\vb{A}$}

    Le potentiel vecteur \emph{retardé},
    définit pour une distribution de courants par~:
    \begin{equation*}
        \vb{A}(M, t)
        = \frac{\mu_0}{4\pi} \iiint{\frac{\vb{j}(M', t-\frac{M'M}{c})}{r}\dd V}
        \qq{où}
        \vb{j}
        = \vb{v} \rho
    \end{equation*}
    décrit
    le potentiel effectivement ressenti
    au point $r$
    à l'instant $t$,
    alors qu'il a été créé
    auparavent
    par l'ensembles des points de l'espace.
    Dans le cadre
    de l'approximation dipolaire,
    nous aurons~:
    \begin{equation*}
        \vb{A}(M, t)
        = \frac{\mu_0}{4\pi r} \iiint{\vb{j}(M', t-\tfrac{M'M}{c}) \dd V}
    \end{equation*}
    Plus précisément
    dans notre situation,
    comme nous avons seulement
    deux charges ponctuelles ($\pm q$)
    parmis lesquelles
    une seule ($-q$)
    est animée
    d'une vitesse $\dt{\vb{d}}$,
    l'intégrale vaut
    $q\dt{\vb{d}} = \dt{\vb{p}}$.
    Nous aurons
    finalement~:
    \begin{equation*}
        \vb{A}(M, t)
        = \frac{\mu_0}{4\pi} \frac{\dt{\vb{p}}(t-\tfrac{r}{c})}{r}
    \end{equation*}

    \subsubsection{Potentiel scalaire $V$}

    La jauge de \propername{Lorentz}~:
    \begin{equation*}
        \div{\vb{A}}
        = -\frac{1}{c^2}\pdv{V}{t}
    \end{equation*}
    permet de calculer aisément
    le potentiel scalaire $V$
    en effet~:
    \begin{equation*}
        \div{\vb{A}}
        = \pdv{A_z}{z}
        = \pdv{r}{z} \pdv{A_z}{r}
        = \frac{\mu_0\cos(\theta)}{4\pi} \qty(
            \frac{-\dt{p}(t-\tfrac{r}{c})}{r^2}
            - \frac{\dtt{p}(t-\tfrac{r}{c})}{rc}
            )
    \end{equation*}
    d'où~:
    \begin{align*}
        V(M, t)
        &= \frac{\mu_0\cos(\theta)}{4\pi c^2}
            \int \qty(
                \frac{\dt{p}(t-\tfrac{r}{c})}{r^2}
                + \frac{\dtt{p}(t-\tfrac{r}{c})}{rc}
                )
            \dd t
        \\
        &= \frac{\cos(\theta)}{4\pi\epsilon_0} \qty(
                \frac{p(t-\tfrac{r}{c})}{r^2}
                + \frac{\dt{p}(t-\tfrac{r}{c})}{rc}
                )
        \\
        &= \frac{1}{4\pi\epsilon_0} \qty(
                \frac{\vb{p}(t-\tfrac{r}{c})}{r^2}
                + \frac{\dt{\vb{p}}(t-\tfrac{r}{c})}{rc}
                ) \vu{r}
    \end{align*}

    Dans la situation stationnaire,
    $\dt{p} = 0$,
    on retrouve
    les potentiels
    électrique
    et magnétique
    du dipôle électrique~:
    \begin{equation*}
        \vb{A}(M, t)
        = \vb{0}
        \qq{et}
        V(M, t)
        = \frac{1}{4\pi\epsilon_0} \frac{\vb{p}\dotproduct\vu{r}}{r^2}
    \end{equation*}

    \begin{draft}
        \paragraph{Sur l'invariance de jauge}
        Les équations de \propername{Maxwell}~:
        \begin{gather*}
            \div\vb{E} = \frac{\rho}{\epsilon_0}
            \\
            \div\vb{B} = 0
            \\
            \curl\vb{E} = -\pdv{\vb{B}}{t}
            \\
            \curl\vb{B} = \mu_0\vb{j} + \mu_0\epsilon_0 \pdv{\vb{E}}{t}
        \end{gather*}

        Permettent de définir $V$ et $\vb{A}$~:
        \begin{gather*}
            \div{\vb{B}} = \vb{0}
            \implies
            \exists \vb{A} \suchas \curl{A} = \vb{B}
            \\
            \curl{\vb{E}} = - \pdv{\vb{B}}{t}
            \implies
            \curl(\vb{E} + \pdv{\vb{A}}{t}) = \vb{0}
            \implies
            \exists -V \suchas -\div{V} = \vb{E} + \pdv{A}{t}
        \end{gather*}
        de manière non univoque
        puisque~:
        \begin{gather*}
            \vb{A'} = \vb{A} + \grad f
            \implies
            \curl\vb{A'} = \curl\vb{A} + \curl\grad f = \curl\vb{A}
            \\
            \qq{laisse}\vb{B}\qq{inchangé}
            \\
            \qq{puis}
            \vb{E} = -\grad{V} - \pdv{\vb{A}}{t} - \pdv{\grad f}{t}
            \implies
            \vb{E} = -\grad(V+\pdv{f}{t}) - \pdv{A}{t}
            \\
            \qq{laisse}\vb{E}\qq{inchangé sous la condition}V' = V + \pdv{f}{t}
            \\
            \vb{E}\qq{et}\vb{B}\qq{inchangés = physique inchangée}
        \end{gather*}
        Le choix de $f$
        est un choix de jauge~;
        et comme~:
        \begin{equation*}
            \div\vb{A'}
            = \div\vb{A} + \laplacian f
        \end{equation*}
        le choix de $f$
        peut être fait indirectement
        en faisant un choix de $\div\vb{A'}$~:
        choisir $\div\vb{A'}$
        c'est choisir une jauge.

        On peut écrire d'une part~:
        \begin{equation*}
            \div\vb{E}
            = \div(-\grad V - \pdv{\vb{A}}{t})
            = - \laplacian V - \pdv{t}(\div\vb{A})
            = \frac{\rho}{\epsilon_0}
        \end{equation*}
        et d'autre part~:
        \begin{align*}
            \curl(\curl\vb{A})
            &= \grad(\div\vb{A}) - \laplacian\vb{A}
            \\
            &= \mu_0\vb{j} + \mu_0\epsilon_0\pdv{\vb{E}}{t}
            = \mu_0\vb{j} + \mu_0\epsilon_0\pdv{t}(-\grad V - \pdv{\vb{A}}{t})
            \\
            \implies
            \epsilon_0\mu_0 \pdv[2]{\vb{A}}{t} - \laplacian\vb{A}
            &= \mu_0\vb{j} - \grad(\mu_0\epsilon_0 \pdv{V}{t} + \div\vb{A})
        \end{align*}

        On remarque que,
        si l'on avait
        $f$
        telle que~:
        \begin{equation*}
            \div\vb{A'} + \mu_0\epsilon_0\pdv{V'}{t} = 0
        \end{equation*}
        alors
        ce serait bien
        puisqu'on aurait~:
        \begin{align*}
            \mu_0\epsilon_0\pdv[2]{V'}{t} - \laplacian V'
            &= \frac{\rho}{\epsilon_0}
            \\
            \qq{et}
            \mu_0\epsilon_0\pdv[2]{\vb{A'}}{t} - \laplacian \vb{A'}
            &= \mu_0\vb{j}
        \end{align*}
        équation symétriques.
        On choisir donc la jauge, dite de \propername{Lorentz}~:
        \begin{equation*}
            \div\vb{A'}
            = - \mu_0\epsilon_0 \pdv[2]{V'}{t}
        \end{equation*}
        et sous cette
        les équations symétriques
        sur $\vb{A'}$ et $V'$
        précédentes
        sont vérifiées
        et conforme à la physique,
        au même titre que
        celles sur $\vb{A}$ et $V$.

        Ces équations
        s'écrivent indiférement des quantités physiques~:
        \begin{equation*}
            \pdv[2]{\psi}{t} - c^2 \laplacian\psi = s
        \end{equation*}
        où
        $s$ est un terme «~de source~»
        \dots
        la suite dans \cite[chap. 21.2]{feynmanem1}
    \end{draft}

    \subsection{Champs}

    \subsubsection{Champ magnétique $\vb{B}$}

    Vu la symétrie cylindrique du problème,
    nous proposons un système de coordonnées
    $O(\vu{\rho},\vu{\phi},\vu{z})$
    et
    nous prévoyons que le champ magnétique
    sera orienté selon $\vu{\phi}$.
    Les coordonnées cylindriques
    et les coordonnées sphériques
    sont liées en particulier par~:
    \begin{gather*}
        \vu{\rho}
        = \vu{r} (1 - \cos\theta)
        \implies
        \rho
        = r \sin\theta
    \end{gather*}
    Connaissant
    $\vb{B} = \curl{A}$,
    nous pouvons écrire~:
    \begin{equation*}
        (\curl{A}) \dotproduct \vu{\phi}
        = \pdv{A_r}{z} - \pdv{A_z}{\rho}
        = - \pdv{r}{\rho} \pdv{A_z}{r}
        = \sin(\theta) \frac{\mu_0}{4\pi r^2} \qty(
            \dt{p}
            + \frac{r}{c}\dtt{p}
            )
    \end{equation*}
    et on en tire,
    \begin{equation*}
        \vb{B}(M, t)
        = \frac{\mu_0}{4\pi r^2} \qty(
            \dt{\vb{p}} + \frac{r}{c}\dtt{\vb{p}}
            ) \vu{r}
    \end{equation*}

    \subsubsection{Champ électrique $\vb{E}$}

    Vu la symétrie cylindrique
    autour de $\vu{z}$,
    nous prévoyons que le champ électrique
    n'aura pas de composante
    selon $\vu{\phi}$.
    Avec
    $\vb{E} = - \grad{V} - \pdv{\vb{A}}{t}$
    nous pouvons calculer~:
    \begin{align*}
        E_r(M, t)
        &= - \pdv{V}{r} - \pdv{A_r}{t}
        \\
        &= \frac{\cos(\theta)}{4\pi\epsilon_0} \qty(
            \frac{-\dt{p}}{r^2c}
            + \frac{-2p}{r^3}
            + \frac{-\dtt{p}}{rc^2}
            + \frac{-\dt{p}}{r^2c}
            )
            - \frac{\mu_0}{4\pi}\frac{\dtt{p}\cos(\theta)}{r}
        \\
        &= \frac{\cos(\theta)}{4\pi\epsilon_0} \qty(
            \frac{2p}{r^3}
            + \frac{2\dt{p}}{r^2c}
            )
    \end{align*}
    puis,
    \begin{align*}
        E_\theta(M, t)
        &= - \frac{1}{r}\pdv{V}{\theta} - \pdv{A_\theta}{t}
        \\
        &= \frac{\sin(\theta)}{4\pi\epsilon_0 r^3} \qty(p + \frac{r}{c}\dt{p})
            - \frac{\mu_0}{4\pi}\frac{\dtt{p}\sin(\theta)}{r}
        \\
        &= \frac{\sin(\theta)}{4\pi\epsilon_0} \qty(
            \frac{p}{r^3}
            + \frac{\dt{p}}{r^2c}
            + \frac{\dtt{p}}{rc^2}
            )
    \end{align*}

    Remarquons que
    $\vb{E}$(M, t) et $\vb{B}$(M, t)
    sont orthogonaux.

    \subsection{Régime harmonique}

    Lorsque $a(t) = a_0 \cos(\omega t)$,
    le passage
    en représentation complexe
    des grandeurs périodiques
    va permettre d'écrire~:
    \begin{equation*}
        \z{p}(t) = p_0 e^{-i \omega t}
        \qq{}
        \z{p}(t' = t - \tfrac{r}{c}) = p_0 e^{-i \omega (t - \frac{r}{c})}
        \qq{}
        \dt{\z{p}} = -i \omega \z{p}
        \qq{}
        \dtt{\z{p}} = - \omega^2 \z{p}
    \end{equation*}
    ce qui donnera~:
    \begin{align*}
        \z{B_\phi}(M, t)
        &= \frac{\sin\theta}{4\pi\epsilon_0 c^2}
            \qty(\frac{-i\omega}{r^2} - \frac{\omega^2}{rc}) \z{p}
        \\
        \z{E_r}(M, t)
        &= \frac{2\cos\theta}{4\pi\epsilon_0} \qty(
            \frac{1}{r^3}
            + \frac{-i\omega}{r^2c}
            ) \z{p}
        \\
        \z{E_\theta}(M, t)
        &= \frac{\sin\theta}{4\pi\epsilon_0} \qty(
            \frac{1}{r^3}
            - \frac{i\omega}{r^2c}
            - \frac{\omega^2}{rc^2}
            ) \z{p}
    \end{align*}

\section{Rayonnement à grande distance}

    Si l'on se place assez près du dipôle,
    de sorte que
    la durée de propagation
    soit petite
    devant la période de l'onde émise~:
    \begin{equation*}
        \frac{r}{c}
        \ll \frac{2\pi}{\omega}
        = \frac{2\pi}{\flatfrac{c}{\lambda}}
        \implies
        r
        \ll \lambda
    \end{equation*}
    mais en vérifiant toujours l'approximation dipolaire
    donc~:
    \begin{equation*}
        d \ll r \ll \lambda
    \end{equation*}
    les termes en $\flatfrac{1}{r^3}$
    seront dominants.
    Nous nous intéressons au cas contraire.

    \subsection{Champs à grande distance}

    À grande distance,
    $r \gg \lambda$,
    les termes dominants
    sont ceux en $\flatfrac{1}{r}$.
    Nous aurons,
    \begin{align*}
        \z{B_\phi}(M, t)
        &= - \frac{\sin\theta}{4\pi\epsilon_0} \frac{\omega^2}{rc^3} \z{p}
        \\
        \z{E_r}(M, t)
        &= 0
        \\
        \z{E_\theta}(M, t)
        &= - \frac{\sin\theta}{4\pi\epsilon_0} \frac{\omega^2}{rc^2} \z{p}
    \end{align*}
    ce qui s'écrit vectoriellement~:
    \begin{align*}
        \z{\vb{B}}(M, t)
        &= - \frac{\sin\theta}{4\pi\epsilon_0} \frac{\omega^2}{rc^3} \z{p}(t')
            \vu{\phi}
        \\
        \z{\vb{E}}(M, t)
        &= - \frac{\sin\theta}{4\pi\epsilon_0} \frac{\omega^2}{rc^2} \z{p}(t')
            \vu{\theta}
    \end{align*}
    On on rapelle~:
    $t' = t - \frac{r}{c}$.
    Le signe $-$
    n'est qu'un terme de phase
    vis-à-vis
    de la polarisation $p$.
    Concernant
    les amplitudes~:
    $\norm{\z{\vb{B}}} = \flatfrac{\norm{\z{\vb{E}}}}{c} \vu{\phi}$.

    Les vecteur
    champ électrique
    et champ magnétique
    sont en phase,
    ils forment un trièdre direct avec le vecteur $\vu{r}$,
    et leur amplitude
    décroit en $\flatfrac{1}{r}$
    à grande distance du dipôle~:
    localement,
    nous avons une structure d'onde plane
    puisque le champ est presque uniforme.
    Le plan d'onde associé
    est tangeant à la sphère
    de rayon $r$
    et centrée en $O$.

    On peut tracer
    l'amplitude de l'un des champs
    en coordonnées polaires,
    \begin{todo}
        Le faire~!
    \end{todo}

    \subsection{Puissance rayonnée}

    \subsubsection{Vecteur de \propername{Poynting}}

    On définit le vecteur de \propername{Poynting}~:
    \begin{equation*}
        \vb{R}
        \defeq \frac{\vb{E} \crossproduct \vb{B}}{\mu_0}
        = \epsilon_0 c \norm{\vb{E}}^2 \vu{r}
        = \frac{\sin[2](\theta)}{16\pi^2\epsilon_0 c^3}
        \frac{ \omega^4 p_0^2}{r^2}
            \cos(\omega t')
            \vu{r}
    \end{equation*}
    homogène à
    des \si{\watt\per\meter\squared},
    qui indique
    la direction
    et le flux d'énergie portée
    par l'onde électromagnétique.
    Les détecteurs
    sont généralement sensibles
    à sa moyenne temporelle
    $\avg{\vb{R}(M)}$~:
    \begin{equation*}
        \avg{\vb{R}(M)}
        = \frac{1}{32\pi^2\epsilon_0 c^3}
            \qty(\frac{\omega^2 p_0 \sin\theta}{r})^2
            \vu{r}
    \end{equation*}
    L'énergie
    est ainsi véhiculée
    selon l'axe radial $\vu{r}$,
    et son flux
    décroit en $\flatfrac{1}{r^2}$.
    Ce dernier est,
    en particulier,
    proportionel à
    $\sin[2](\theta)$,
    donc nul
    le long de l'axe $\vu{z} \parallel \vb{p}$ du dipôle.
    On peut maintenant
    tracer un diagramme de rayonnement,
    regroupant ces informations
    sur un graphe~:
    \begin{todo}
        Le faire~!
    \end{todo}

    \subsubsection{Puissance totale rayonnée}

    La totalité
    de la puissance rayonnée
    traverse
    chaque surface sphérique
    de rayon $r$
    centrée en $O$,
    notée $\mathcal{S}(r)$.
    On calculera donc
    $\mathcal{P}$ la puissance moyenne~:
    \begin{align*}
        \mathcal{P}
        = \oint_\mathcal{S} \avg{\vb{R}} \dotproduct \dd\vb{\mathcal{S}}
        &= \frac{\omega^4 p_0^2}{32\pi^2\epsilon_0 c^3}
            \int_{0}^{\pi}
            \int_{0}^{2\pi}
            \qty(\frac{\sin\theta}{r})^2
            r^2 \sin\theta \dd\theta \dd\phi
        \\
        &= \frac{\omega^4 p_0^2}{16\pi\epsilon_0 c^3}
            \int_{0}^{\pi} \dd\theta
            \sin[3](\theta)
        \\
        &= \frac{1}{4\pi\epsilon_0} \frac{\omega^4 p_0^2}{3 c^3}
    \end{align*}
    Le résultat
    ne dépend,
    bien sûr,
    pas de $r$.

\section{Rayonnement dipolaire d'un électron atomique}

    \subsection{Description classique de l'atome}

    Le modèle choisi
    du dipôle électrique
    n'est pas anodin
    en effet,
    les atomes
    constitués de noyaux
    et d'électrons
    (plus légers, $\flatfrac{m_n}{m_e} \approx \num{2000}$)
    vont avoir un comportement
    proche de celui des dipôles.
    Dans le modèle
    de l'électron élastiquement lié,
    on définit
    une force de rappel
    liée à l'interaction coulombienne
    entre le noyau et l'électron considéré,
    on montre que
    l'électron oscille
    autour d'une position d'équilibre
    à une pulsation $\omega_0$.

    Dans ce modèle,
    nous sommes donc contraint
    de considérer le rayonnement
    émis par l'électron.
    La puissance moyenne rayonnée
    est~:
    \begin{equation*}
        \mathcal{P}
        = \frac{1}{4\pi\epsilon_0} \frac{\omega_0^4 (-e x_0)^2}{3c^3}
    \end{equation*}
    avec
    $-e$ la charge de l'électron
    et
    $x_0$ l'élongation de l'oscillateur.
    On remarquera
    que pour l'oscillateur harmonique,
    l'accélération moyenne
    s'écrit
    $a = \avg{\dtt{x_0}} = \flatfrac{- \omega_0^2 x_0}{2}$
    donc
    $\omega^4 x_0^2 \propto a^2$
    d'où
    $\mathcal{P} \propto a^2$.
    Ce résultat
    se généralise
    au mouvement quelconques
    de charges accélérées,
    établi par \propername{Larmor}
    en 1897,
    il explique
    le rayonnement émis
    par les particules dans les synchrotrons.

    L'énergie émise
    résulte d'une transformation
    de l'énergie mécanique~:
    \begin{equation*}
        \mathcal{E}_m
        = \frac{1}{2} m_e (\omega_0 x_0)^2
        = \frac{12 \pi \epsilon_0 c^3}{\omega_0^2} \mathcal{P}
    \end{equation*}
    qui diminue
    selon~:
    \begin{equation*}
        - \dd \mathcal{E}_m
        = \mathcal{P} \dd t
        = \frac{\omega_0^2}{12 \pi \epsilon_0 c^3} \mathcal{E}_m \dd t
        \defeq \frac{1}{\tau} \mathcal{E}_m \dd t
        \implies
        - \frac{\dd \mathcal{E}_m}{\mathcal{E}_m}
        = \frac{\dd t}{\tau}
        \implies
        \mathcal{E}_m(t)
        = \mathcal{E}_{m}(0) e^{\flatfrac{-t}{\tau}}
    \end{equation*}
    Le phénomène
    d'amortissement mécanique
    est souvent modélisé
    par une force de frottement fluide ($\propto \dt{x_0}$),
    dont les conséquences
    sur le mouvement
    seraient identiques.
    On ne peut toutefois pas conclure
    sur l'émission de l'atome
    sans valeur de $x_0$.

    \subsection{Diffusion du rayonement par un atome}

    On considère encore
    un électron élastiquement lié et amorti
    dont l'équation du mouvement
    en régime libre s'écrit~:
    \begin{equation*}
        \dtt{\z{x}} + \frac{1}{\tau} \dt{\z{x}} + \omega_0^2 \z{x}
        = 0
    \end{equation*}
    lorsque l'électron
    est soumis à un champ électrique $\vb{E}$
    de pulsation $\omega$
    et d'amplitude $E_0$,
    l'équation devient~:
    \begin{equation*}
        \dtt{\z{x}} + \frac{1}{\tau} \dt{\z{x}} + \omega_0^2 \z{x}
        = -e \frac{E_0}{m_e} e^{-i \omega t}
    \end{equation*}
    on reconnait
    l'oscillateur harmonique amorti
    en régime forcé
    pour lequel
    le mouvement est décrit par~:
    \begin{equation*}
        \z{x}
        = \frac{\flatfrac{-e E_0}{m_e}}
            {(\omega_0^2 - \omega^2) - i\flatfrac{\omega}{\tau}}
            e^{-i\omega t}
    \end{equation*}
    On comprend là
    que nous pourrons déterminer
    par une expérience de résonance,
    les valeurs de
    $\omega_0$,
    $\tau$,
    et donc $x_0$.
    En effet,
    la puissance moyenne rayonnée
    par ce dipôle
    s'écrit~:
    \begin{equation*}
        \mathcal{P}
        = \frac{1}{4\pi\epsilon_0} \frac{\omega_0^4 (-e \abs{\z{x}})^2}{3c^3}
        \propto
        \frac{\omega^4}{(\omega_0^2-\omega^2)^2 + \qty(\frac{\omega}{\tau})^2}
    \end{equation*}
    \pyimgen{diffusion-dipole}
    On distingue
    trois domaines~:
    \begin{itemize}
        \item $\omega \ll \omega_0$
            où $\mathcal{P} \propto \omega^4 \propto \frac{1}{\lambda^4}$,
            domaine de
            la diffusion de \propername{Rayleigh},
        \item $\omega \gg \omega_0$
            où $\mathcal{P} \approx \cst$,
            domaine de la diffusion de \propername{Compton},
        \item et le domaine intermédiaire,
            de diffusion résonante.
    \end{itemize}
    Les pulsations
    de lumière visible
    dans l'atmosphère
    se situent
    dans le domaine de diffusion de \propername{Rayleigh}
    ($\omega_0 \approx \SI{1e17}{\radian\per\second}$
    \cite{perezelectromag}),
    les couleurs
    de \emph{courte}
    longueurs d'onde
    sont donc \emph{moins}
    diffusées
    que celles
    de longue
    longueur d'onde.
