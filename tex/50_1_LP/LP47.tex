\chapter{Mécanismes de la conduction électrique dans les solides}

Dans cette leçon
nous allons nous intéresser
à l'origine microscopique
du courant électrique.
Nous verrons que l'on peu
à partir d'un modèle simple
retrouver la loi d'\propername{Ohm},
mais ce modèle
sera rapidement mis en défaut
par des résultats expérimentaux
plus généraux.

\section{Modèle classique de la conduction électrique \cite{ashcroft}}

    \subsection{Courant et densité de courant}

    Dès le collège
    on apprend que
    le courant électrique
    est dû
    à un déplacement de charges
    portées par des électrons mobiles.
    Plus formellement,
    l'intensité $i$ du courant électrique
    c'est
    la quantité de charges
    qui traversent une surface
    du conducteur
    par unité de temps.
    Si l'on suppose
    que l'ensemble des électrons
    se déplace à une vitesse moyenne $v$,
    alors~:
    \begin{equation*}
        i
        = \frac{n (-e) (S v \dd t)}{\dd t}
        = - n e S v
    \end{equation*}
    En effet,
    les électrons
    traversant la surface $S$
    entre $t$ et $t + \dd t$
    sont ceux
    qui à l'instant $t$
    se trouvaient
    dans le volume $S v \dd t$.
    On appelle
    \emph{densité de courant}
    la quantité surfacique~:
    \begin{equation*}
        j
        = - n e v
        \implies
        i
        = \iint_S j \dd S
    \end{equation*}
    l'intéret
    d'introduire cette grandeur
    étant de pouvoir étudier
    des situations
    où la densité de courant
    sur une surface
    n'est pas constante.
    \begin{todo}
        Faire un dessin
        des électrons
        qui traversent la section.
    \end{todo}
    On peut rechercher
    une expression théorique
    de la densité de courant
    (en fait ici, de la vitesse moyenne $v$)
    en appliquant
    la mécanique classique
    aux électrons,
    on se demande alors
    quels sont les forces
    exercées sur ces derniers.

    \subsection{Hypothèses de \propername{Drude}}

    \subsubsection{Origine des électrons}

    D'abord
    il convient
    de discuter l'origine
    des électrons
    présents
    et libres de se déplacer
    dans le milieu.
    Pour un métal,
    nous supposerons que
    les électrons de valence
    engagés dans les liaisons métalliques
    sont très peu liés
    à leur atome «~d'origine~»
    et donc
    susceptibles de se déplacer
    sur plusieurs distances
    interatomiques.
    Éventuellement,
    l'ensemble des électrons
    peut se «~décaler~»
    par rapport à
    l'ensemble des atomes «~d'origine~».
    On distingue ainsi
    les électrons \emph{de conduction}
    des électrons \emph{liés}.
    Les électrons de conduction
    sont susceptibles
    de se déplacer
    au coeur d'un réseau d'ions.

    \begin{todo}
        Faire un petit schéma
        ou une animation
        avec les électrons de liaison
        et les électrons de coeur.
    \end{todo}

    \subsubsection{Principe fondamental de la dynamique}

    Pour mettre en mouvement
    les électrons,
    nous appliquons
    une différence de potentiel $U$
    aux extrémités du conducteur.
    Cette différence de potentiel
    se traduit
    dans le millieu
    par l'existence
    d'un champ électrique
    $\vb{E}$.
    Les électrons
    étant en mouvement,
    nous considérons
    dans un premier temps
    égallement l'existence
    d'un champ magnétique $\vb{B}$.

    Si les électrons
    étaient totalement libres,
    alors clairement
    le principe fondamental de la dynamique
    indiviudel
    s'écrirait~:
    \begin{equation*}
        m \dv{\vb{u}_i}{t}
        = - e \qty(\vb{E} + \vb{u}_i \crossproduct \vb{B})
        \approx - e \vb{E}
    \end{equation*}

    Les électrons seraient donc
    animés d'un mouvement
    rectiligne uniformément acceléré.
    L'hypothèse
    d'électrons totalement libres
    est une hypothèse assez forte,
    et l'on constate
    qu'elle même à un problème~:
    le courant
    ne devrait cesser d'augmenter.

    Mais nous l'avons dit
    les électrons
    évoluent dans un réseau d'ions~:
    ils sont donc soumis
    à des forces électrostatiques
    et éventuellement
    des forces de contact.
    N'oublions pas non plus
    les interactions inter-électrons.

    \propername{Drude} (en 1900),
    dans le contexte
    de la découverte des électrons (\propername{Thomson}, 1897)
    propose d'étudier le problème
    par assimilation
    à la théorie cinétique des gaz (1800).

    Nous supposerons
    que les forces électrostatiques
    sont peu importantes
    devant celle exercés
    par le champ $\vb{E}$ extérieur.
    Concernant
    les forces de contact
    c'est à dire
    les chocs
    entre les électrons
    et les ions du réseau~;
    nous les modéliserons
    par une force statistique.
    L'idée est de dire que,
    en moyenne,
    les chocs corresponent
    à des échanges aléatoires
    de quantité de mouvement.
    Un électron
    qui à une vitesse $\vb{u}_i'$
    juste avant un choc
    aura une vitesse $\vb{u}_i'$
    qui en est complètement indépente
    juste après le choc.
    Dans ce cadre
    nous pouvons dire que
    chaque électron
    se comporte
    comme un électron libre
    entre le chocs,
    qui sont en moyenne
    espacés d'une durée $\tau$.
    La quantité de mouvement
    aquise par chaque électrons
    entre deux chocs
    s'écrit alors~:
    \begin{equation*}
        m \qty(\vb{u}_i(t) - \vb{u}_i(t - \tau))
        = - e \vb{E} \tau
    \end{equation*}
    Tenir compte
    de l'aspect aléatoire de chaque choc
    posera problème mais,
    sur l'ensemble des électrons
    cette composante de la vitesse
    va disparaître~:
    \begin{equation*}
        \vb{v}(t)
        = \avg{\vb{u}_i(t)}
        = \avg{\vb{u}_i(t - \tau) - \frac{e}{m}\vb{E}\tau}
        = \frac{-e\tau}{m}\vb{E}
    \end{equation*}

    Sous ces hypothèses,
    l'ensemble des électrons
    sous l'infulence du champ $\vb{E}$
    est donc accéléré
    jusqu'à une vitesse limite.
    Le problème
    est très analogue à celui
    de la chute d'un corps
    dans un fluide visqueux.

    \subsection{Loi d'\propername{Ohm} locale, loi d'\propername{Ohm}}

    De la vitesse $v = \norm{\vb{v}}$,
    on peut exprimer
    la densité de courant~:
    \begin{equation*}
        \vb{j}
        = - n e \vb{v}
        = \frac{ne^2\tau}{m}\vb{E}
        \defeq \sigma \vb{E}
    \end{equation*}
    c'est la loi d'\propername{Ohm} locale,
    $\sigma$ est
    la \emph{conductivité électrique}.

    On peut
    calculer l'intensité du courant $i$
    à travers le conducteur
    de surface $S$
    et de longueur $L$~:
    \begin{equation*}
        i
        = \sigma \norm{\vb{E}} S
        = \sigma \norm{-\grad{V}} S
        = \sigma \frac{S}{L} U
        = G U
    \end{equation*}
    $\sigma$ est la conductivité,
    $G = \sigma \flatfrac{S}{L}$ la conductance.
    Sous une forme plus habituelle,
    on a trouvé
    la loi d'\propername{Ohm}~:
    \begin{equation*}
        U
        = \frac{i}{G}
        = R i
    \end{equation*}
    avec
    une forme connue pour la résistance
    $R = \rho \flatfrac{L}{S}$
    où
    $\rho$ est la résistivité.
    On retrouve bien
    les dépendances attendues
    vis-à-vis
    des dimensions géométriques.

    Résumons~:
    \begin{gather*}
        \vb{j}
        = \frac{ne^2\tau}{m}\vb{E}
        \\
        U = \frac{L}{S} \frac{m}{ne^2\tau} i
    \end{gather*}

    \subsection{Validation du modèle}

    \subsubsection{Valeur de la conductivité}

    Pour valider notre modèle,
    on doit pouvoir donner une valeur
    à $\tau$,
    temps moyen entre deux chocs,
    qui est directement lié
    au libre parcourt moyen $l$
    par $v \tau = l$.
    Il faudra donner une valeur à $v$.

    Il semble raisonable
    de prendre pour ordre de grandeur de $l$
    la distance inter ions,
    prenons $l = n^{\flatfrac{2}{3}} \approx \SI{1}{\angstrom}$.

    \propername{Drude}
    a appliqué
    la statistique
    de la théorie cinétique des gaz
    (\propername{Maxwell-Boltzmann}) aux électrons
    de sorte à affirmer,
    en ordre de grandeur~:
    \begin{equation*}
        \frac{1}{2} m v^2
        = \frac{3}{2} k_B T
    \end{equation*}
    pour la vitesse moyenne
    des électrons
    dans un métal
    à la température $T$.
    Finalement,
    \begin{equation*}
        \sigma
        = \frac{n^{\flatfrac{2}{3}} e^2}{\sqrt{3 k_B T m}}
    \end{equation*}

    En pratique
    on tablue plutôt
    la résestivité $\rho = \flatfrac{1}{\sigma}$~:
    l'application numérique
    pour le cuivre donne
    une valeur de
    $\rho_{\ce{Cu}} = \SI{21e-8}{\ohm\meter}$,
    avec la densité $n = \SI{8.47e22}{\per\centi\meter\cubed}$
    à \SI{273}{\kelvin}.
    La valeur
    est à comparer
    avec la valeur expérimentale~:
    $\rho_{\ce{Cu}}^{\mathrm{exp}} = \SI{1.58e-8}{\ohm\meter}$.
    En plus,
    notre modèle
    prévoit
    $\rho \propto \sqrt{T}$
    or,
    expérimentalement
    on constate plutôt
    $\rho^{\mathrm{exp}} \propto T$.

    \begin{draft}
        En fait
        \cite{drudeepfl},
        la statistique de \propername{Maxwell-Boltzmann}
        ne s'applique pas
        en raison de la nature quantique
        des électrons.
        Cette correction
        au modèle de \propername{Drude}
        constitue
        le modèle de \propername{Sommerfeld}.

        En appliquant
        la statistique de \propername{Fermi-Dirac}
		pour les fermions
        on trouve que~:
        \begin{itemize}
            \item la vitesse moyenne $v$
                ne dépend pas de la température
            \item le libre parcourt moyen
                est de l'ordre de \SI{300}{\angstrom}
                à température ambiante
        \end{itemize}
		la dépendance
		de la résistivié
		à la température
		est linéaire
		et liée aux impuretés
        et aux phonons
        qui traduisent les vibrations du réseau
        (voir \cite[chap. VI]{kittel}).
    \end{draft}

    \pyimgen{conductivite_metaux}

    \subsubsection{Mesure de la densité des porteurs de charge}

    \begin{pydo}
        On peut imaginer
        une animation interactive
        pour expliquer l'effet \propername{Hall}
        dans laquelle
        on peut envoyer les électrons
        un-à-un.
        Mais c'est un gros travail\dots
    \end{pydo}

    \begin{manip}
        Mesure avec l'effet \propername{Hall}
        sur un semi-conducteur.
    \end{manip}

    \begin{todo}
        Faire le calcul
        et bien expliquer l'expérience.
    \end{todo}

    \begin{draft}
        Faire remarquer
        que les ions du réseau
        sont eux aussi
        soumis au champ de \propername{Hall},
        et que c'est ce champ
        qui donne lieu à la force de \propername{Laplace}.
    \end{draft}

    On constate
    dans cette expérience
    que les porteurs de charge sont positifs\dots

    \subsubsection{Autres validations}

    Nous pouvons citer
    quelque autres problèmes
    associés à ce modèle \cite{ashcroft}~:
    \begin{itemize}
        \item Si l'on considère
            les électrons comme quasi-libres
            dans le métal
            alors
            leur énergie ($\frac{1}{2}m\vb{v}^2$)
            doit compter pour $\frac{3}{2} k_B T$
            dans l'expression de
            la capacité thermique $c_v$
            du métal.
            Expérimentalement,
            on mesure $c_v \propto \frac{6}{2} k_B T$,
            ce qui correspond
            uniquement aux ions.
        \item Nous distinguons
            les métaux
            des isolants
            par le type de liaison chimique
            engagées entre les atomes
            mais,
            nous n'avons aucun moyen
            de prévoir le comportement
            d'un matériaux donné.
    \end{itemize}

    \begin{todo}
        Il faut dire
        quelque mots
        sur la conduction
        en courant alternarif
        \cite{ashcroft}.
    \end{todo}

\section{\dots}

    \begin{todo}
        À décider entre
        le modèle de \propername{Sommerfeld}
        et la théorie des bandes
        \cite{ashcroft, kittel}.

        Le modèle de \propername{Sommerfeld}
        apporte peu dans cette leçon
        car il n'explique pas non plus
        le cas des trous dans les semi-conducteurs.

        La théorie des bandes,
        c'est vite compliqué
        et on risque de ne pas en dire assez.
    \end{todo}
