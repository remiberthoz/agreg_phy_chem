\chapter{Ondes électromagnétiques dans les milieux diélectriques}

\prereqstart
{Électrostatique~: $\vb{D} = \epsilon_0 \vb{E} + \vb{P}$}
{Propagation des ondes électromagnétiques dans le vide}
\prereqstop

Dans le cours d'électrostatique
nous avons vu que
la présence d'un diélectrique
dans un condensateur
modifiait sa capacité,
car le milieu se polarisait
ce qui avait pour effet
de modifier le champ électrique
qui y était présent.

Nous avons déja étudié
la propagation des ondes électromagnétiques
dans le vide.
Puisque
la présence d'un diélectrique
influe sur le champ électrique,
on peut supposer que
la présence d'un diélectrique
va influer sur la propagation des ondes.

\begin{manip}
    On propose d'essayer
    une expérience bien connue~:
    on place un prisme
    sur le chemin
    d'un faiseau lumieux.
\end{manip}

\section{Équations de \propername{Maxwell} dans les milieux diélectriques}

    On rappelle
    les équations de \propername{Maxwell}
    dans la matière~:
    \begin{gather*}
        \div{\vb{D}}
        = \rho_{\textrm{libre}}
        \qq{}
        \curl{\vb{E}}
        = - \pdv{\vb{B}}{t}
        \\
        \div{B} = 0
        \qq{}
        \curl{\vb{H}}
        = \mu_0 \vb{j}_{\textrm{libre}} + \pdv{\vb{D}}{t}
    \end{gather*}
    avec les vecteurs
    \emph{déplacement électrique}
    et
    \emph{excitation magnétique}
    qui sont liés à
    la \emph{polarisation}
    et à
    l'\emph{aimantation}
    selon~:
    \begin{equation*}
        \vb{D}
        = \epsilon_0 \vb{E} + \vb{P}
        \qq{et}
        \vb{H} = \frac{\vb{B}}{\mu_0} - \vb{M}
    \end{equation*}

    Dans un milieu diélectrique
    non magnétique
    parfaitement isolant,
    les seules charges présentes
    sont les charges de polarisation,
    de même que
    les seuls courants présents
    sont les courants de polarisation.
    On peut donc réécrire~:
    \begin{gather*}
        \div\vb{D}
        = 0
        \qq{}
        \curl{\vb{B}}
        = \mu_0 \pdv{\vb{D}}{t}
        \\
        \div\vb{B}
        = 0
        \qq{}
        \curl{\vb{E}}
        = - \pdv{\vb{B}}{t}
    \end{gather*}

    On se trouve ici
    avec un vecteur $\vb{D}$
    qui est la seule différence
    que l'on va faire
    entre le vide
    et les diélectriques.
    On connait
    $\vb{D} = \epsilon_0 \vb{E} + \vb{P}$,
    mais
    nous n'avons pas
    d'expression pour $\vb{P}$.

    On pourrait se lancer
    dans une étude abstraite
    de la propagation des ondes
    en proposant
    une relation de structure
    très générale
    comme~:
    \begin{equation*}
        \vb{P}
        = \epsilon_0 [\chi_1(r, \omega)] \vb{E}
        + \epsilon_0 [\chi_2(r, \omega)] \vb{E} \vb{E}
        + \dots
    \end{equation*}
    mais on préfère
    développer un modèle microscopique
    qui permettra
    de justifier des approximations
    et les nouvelles notions
    (nottament le $\z{n} = n' + i n''$ complexe).

\section{Modèle microscopique de la polarisation}

    \subsection{Modèle de l'électron élastiquement lié
        (polarisation électronique)}

    \begin{draft}
        Aller vite,
        tracer $\chi'$ et $\chi''$.
    \end{draft}

    \begin{todo}
        Donner des ordres de grandeur~!
    \end{todo}

    \subsection{Autres types de polarisation}

    \begin{draft}
        Quelque mots ou des schémas seulement.
    \end{draft}

    \subsubsection{Polarisation ionique}

    \subsubsection{Polarisation de rotation}

\section{Propagation des ondes électromagnétiques dans les diélectriques}

    \subsection{Équation de propagation}

    Pour une onde
    de pulsation $\omega$ connue,
    on peut calculer d'une part~:
    \begin{equation*}
        \curl(\curl{\z{\vb{E}}})
        = \curl(- \pdv{\z{\vb{B}}}{t})
        = - \mu_0 \epsilon(\omega) \pdv[2]{\z{\vb{E}}}{t}
    \end{equation*}
    et écrire d'autre part~:
    \begin{align*}
        \curl(\curl{\z{\vb{E}}})
        &= \grad(\div\z{\vb{E}}) - \laplacian{\z{\vb{E}}}
        \\
        &= \frac{1}{\epsilon} \grad(\div\z{\vb{D}}) - \laplacian{\z{\vb{E}}}
        \\
        &= - \laplacian{\z{\vb{E}}}
    \end{align*}
    on obtient alors
    une équation de \propername{D'Alembert}
    pour le vecteur $\z{\vb{E}}$~:
    \begin{equation*}
        \laplacian{\z{\vb{E}}}
        - \mu_0 \epsilon(\omega) \pdv[2]{\z{\vb{E}}}{t}
        = 0
    \end{equation*}
    Cette relation
    est très similaire
    à celle que l'on trouve
    dans le vide,
    la seule modification
    étant
    la vitesse de propagation~:
    \begin{equation*}
        \laplacian{\z{\vb{E}}}
        - \frac{\epsilon_r(\omega)}{c^2} \pdv[2]{\z{\vb{E}}}{t}
        = 0
    \end{equation*}
    Sous cette fome,
    on lit que
    la composante électrique
    en représentation complexe
    du champ électromagnétique
    se propage
    dans les diélectriques
    à une vitesse~:
    $\z{v} = {c} / {\epsilon^{1/2}_r(\omega)} \defeq c / \z{n}(\omega)$,
    où l'on appelle
    \emph{indice optique complexe}
    la grandeur $\z{n}(\omega)$.
    En calculant
    de la même manière
    $\curl(\curl\vb{B})$
    on trouverait
    la même équation de propagation
    pour la composante  magnétique
    du champ.

    \subsection{Structure des ondes planes}

    Avec l'équation
    de \propername{Maxwell-Faraday}~:
    \begin{equation*}
        \curl\vb{E}
        = - \pdv{\vb{B}}{t}
        \implies
        i \z{\vb{k}} \cross \z{\vb{E}}
        = i \omega \z{\vb{B}}
    \end{equation*}
    on trouve que
    les vecteurs
    $\vb{k}$, $\vb{E}$ et $\vb{B}$
    forment un trièdre direct,
    comme dans le vide.
    Puisque $\vb{D}$ et $\vb{E}$
    sont collinéaires,
    $\vb{D}$ forme aussi un trièdre
    avec les deux autres vecteurs.

    \subsection{Dispersion et atténuation}

    On rappelle
    que l'on considère
    des ondes planes
    pour lequelles
    on utilise
    la représentation harmonique~:
    \begin{equation*}
        \z{\vb{E}}
        = \z{\vb{E}}_0 e^{i (\omega t - \z{\vb{k}} \vb{r})}
        \qq{et}
        \z{\vb{B}}
        = \z{\vb{B}}_0 e^{i (\omega t - \z{\vb{k}} \vb{r})}
    \end{equation*}

    Avec l'équation de \propername{D'Alembert}
    qui donne
    la relation de dispersion~:
    \begin{equation*}
        \z{\vb{k}}^2
        = \frac{\z{n}^2(\omega)}{c^2} \omega^2
        \implies
        \z{\vb{k}}
        = \frac{\omega \z{n}(\omega)}{c} \vb{u}
    \end{equation*}
    on obtient
    pour les champs,
    dans le cas d'une propagation
    unidirectionnelle
    où $\z{\vb{k}} \vb{r} = \z{k} r$~:
    \begin{equation*}
        \z{\vb{E}}
        = \z{\vb{E}}_0
            e^{i \omega \qty(t - r \flatfrac{\z{n}(\omega)}{c})}
        \qq{et}
        \z{\vb{B}}
        = \z{\vb{B}}_0
            e^{i \omega \qty(t - r \flatfrac{\z{n}(\omega)}{c})}
    \end{equation*}

    L'indice optique complexe $\z{n}$
    pose problème
    car dans l'équation de \propername{D'Alembert}
    il semble indiquer
    que les champs
    se propagent avec une vitesse complexe.
    Nous allons résoudre ce problème
    avec une interprétation correcte,
    écrivons~:
    $\z{n} = n' - i n''$.
    Dans ce cas~:
    \begin{align*}
        \z{\vb{E}}
        = \z{\vb{E}}_0
            e^{i \omega \qty(t - r \flatfrac{\z{n}(\omega)}{c})}
        &= \z{\vb{E}}_0
            e^{i \omega t}
            e^{- i r \omega \flatfrac{\z{n}(\omega)}{c}}
        \\
        &= \z{\vb{E}}_0
            e^{i \omega t}
            e^{- i r \omega \flatfrac{n'(\omega)}{c}}
            e^{- r \omega \flatfrac{n''(\omega)}{c}}
    \end{align*}
    De cette égalité
    on peut exprimer
    le champ $\vb{E}$
    réel~:
    \begin{equation*}
        \vb{E}
        = E_0 e^{- r \omega \flatfrac{n''(\omega)}{c}}
            \cos(\omega (t - r \tfrac{n'(\omega)}{c}))
    \end{equation*}
    on voit ici
    que la partie imaginaire
    de l'indice optique complexe
    correspond
    à un coefficient d'atténuation
    de l'amplitude du champ électrique.

    On peut aussi écrire
    l'équation de \propername{D'Alembert}
    pour le champ électrique~:
    \begin{equation*}
        \laplacian{\vb{E}} - \frac{n'^2(\omega)}{c^2} \pdv[2]{\vb{E}}{t}
        = 0
    \end{equation*}
    et voire que
    la partie réelle
    de l'indice optique complexe
    correspond
    à l'indice optique de l'optique géométrique,
    qui donne la vitesse de propagation
    du champ électrique~:
    $v(\omega) = \frac{c}{n'(\omega)}$.
    Puisque $v$
    dépend de $\omega$,
    on peut dire
    que le milieu est dispersif,
    la relation de dispersion étant~:
    $k'(\omega) = \frac{\omega}{c} n'(\omega)$.

\section{Développement limité dans le visible~: loi de \propername{Cauchy}}

    \begin{draft}
        $\omega \ll \omega_0$
    \end{draft}
