\chapter{Aspect corpusculaire du rayonnement, notion de photons}

\niveau{L3}

\prereqstart
{Corps noir et loi de \propername{Planck}}
{Électrostatique~: principe de l'électroscope}
{Relativité restreinte~: quadrivecteur, conservations}
\prereqstop

Dans cette leçon
nous étudions les expériences
qui ont mené les physiciens
à émettre l'hypothèse,
puis valider le modèle
de l'existence d'une particule de lumière porteuse
d'énergie
et de quantité de mouvement~:
le photon.
Il s'agit des tout début
de la mécanique quantique.

\section{Constante de \propername{Planck}, quantification du rayonnement}

    \subsection{Rayonnement du corps noir \cite{dufour}}

    \begin{draft}
        Voir leçon sur le corps noir.
    \end{draft}
    Dans les années 1880,
    les premières mesures quantitatives
    de spectres électromagnétiques
    révélèrent que tous les corps chauds
    émettaient de la lumière
    selon un profil similaire.
    En 1896, \propername{Wien}
    propose un modèle empirique
    valide pour les hautes fréquences.
    En 1900, \propername{Rayleigh}
    propose un modèle théorique
    qui est conforme à l'expérience pour les basses fréquences.

    \propername{Planck},
    qui cherchait une solution au problème
    se rendit compte que s'il faisait l'hypothèse
    selon laquelle l'énergie associée
    au rayonnement de fréquence $\nu$
    était quantifiée
    sous la forme de paquets d'énergie~:
    \begin{equation*}
        \epsilon
        = h \nu
    \end{equation*}
    (avec $h = \SI{6.62e-34}{\joule\second}$,
    la constante ajourd'hui dite,
    constante de \propername{Planck})
    alors le modèle de \propername{Rayleigh}
    devenait valide.

    Cette hypothèse n'avait aucun fondement théoriques,
    \propername{Planck} lui même
    n'était alors pas convaincu par le modèle
    qui impliquait que les transferts d'énergie par rayonnement
    se faisaient de manière discrète
    comme par un échange de particules.

    \subsection{Effet photoélectrique \cite{dufour} \cite{dunodpcsi}}

    \subsubsection{Préambule}

    \propername{Hertz},
    qui faisait des expériences d'électrostatique
    (en 1887)
    se rendit compte que
    s'il éclairait un électroscope
    chargé négativement
    avec de la lumière du domaine ultraviolet,
    les feuilles d'or se rapprochaient.
    S'il éclairait un électroscope
    chargé positivement
    avec la même lumière,
    les feuilles d'or s'éloignaient.

    Les physiciens analysant le phénomène
    émirent une hypothèse
    selon laquelle le rayonnement UV
    pouvait arracher des électrons au métal.

    \subsubsection{Description}

    En 1905,
    \propername{Lenard} étudia ce problème
    et fit des expériences quantitatives.

    \begin{media}
        \url{https://phet.colorado.edu/en/simulation/photoelectric}
    \end{media}

    On place deux plaques métalliques
    face à face dans une enceinte à vide~;
    un générateur de tension
    permet d'appliquer une tension réglable $U$
    entre les deux plaques (qui sont donc des électrodes).
    L'ensemble constitue
    une \emph{cellule photoélectrique}.
    On mesure le courant entre les plaques
    grâce à un ampèremètre,
    et on éclaire l'une des plaques (définie comme cathode)
    avec une source monochromatique réglable (fréquence $\nu$).

    La première expérience
    consiste à se placer dans le cas où
    $U = 0$ (pas de générateur).
    On constate
    qu'il existe une fréquence seuil $\nu_0$
    à partir de laquelle
    le courant passe
    avec une discontinuité
    de $i = 0$
    à $i > 0$.

    \subsubsection{Mesures}

    Si les électrons
    se déplacent de la cathode à l'anode,
    c'est qu'ils sont porteurs
    d'une certaine énergie cinétique.
    L'utilisation du générateur de tension
    va permettre de générer un champ électrique
    entre les électrodes,
    et donc d'accélérer
    ou freiner les électrons.
    Si l'on trouve une tension limite $U_s$
    pour laquelle le courant $i$ devient nul,
    nous pourrons mesurer
    l'énergie cinétique
    qu'ont les électrons
    au moment
    où ils quittent la cathode.
    Effectuons un bilan d'énergie
    sur les électrons (charge $-q$)~:
    \begin{equation*}
        E_{c, C} + E_{p, C}
        = E_{c, A} + E_{p, A}
        \implies
        E_{c, C} - q V_c
        = E_{c, A} - q V_A
        \implies
        E_{c, A}
        = E_{c, C} + q U
    \end{equation*}
    On notera~: $T \defeq E_{c, C}$.
    On se demande à quel moment
    un courant est mesuré~:
    pour mesurer un courant,
    il faut que les électrons arrivent jusqu'à l'anode.
    Autrement dit,
    \begin{equation*}
        E_{c, A}
        \geq 0
        \implies
        U
        \geq \flatfrac{- T}{q}
    \end{equation*}
    Si l'inégalité n'est pas vérifiée
    c'est que les électrons
    ne sont pas parvenus jusqu'à l'anode.
    Nous trouvons donc
    un moyen expérimental
    de mesurer $T$~:
    \begin{equation*}
        T
        = - q U_s
    \end{equation*}
    avec $U_s$ la tension $U$ seuille
    \emph{sous} laquelle il y a le basculement
    de $i > 0$ vers $i = 0$.

    Comme expérimentateur,
    nous avons un contrôle sur le potentiel $U$
    qui créé entre les électrodes
    une différence d'énergie potentielle
    pour les électrons
    $\Delta = - q U$.
    Concrètement,
    pour $T > \Delta$
    on mesure un courant électrique,
    et pour $T < \Delta$,
    il n'y a pas de courant.
    Tout autre paramètre fixés,
    la transition se fait
    à $\Delta_s = - q U_s$.

    L'énergie $T$ correspond à
    l'énergie cinétique des électrons
    qui viennent d'être arrachés du métal,
    elle leur a été fournie
    par la lumière incidente
    alors,
    par conservation de l'énergie
    on écrit~:
    \begin{equation*}
        E_{fournie}
        = T + E_{liaison}
    \end{equation*}

    Notons que,
    si $E_{fournie} < E_{liaison}$
    alors nécessairement $T = 0$
    car l'électron n'est pas arraché du métal.
    Cela explique
    l'existence d'une fréquence seuil $\nu_0$
    si l'on considère que
    l'énergie du rayonnement
    augmente avec sa fréquence.

    L'énergie de liaison
    entre les électrons et le métal
    ne dépend que de ce dernier~:
    nous la traiterons comme un constante
    donc,
    \begin{equation*}
        E_{fournie}
        \defeq E_f
        = T + \cst
    \end{equation*}

    \subsubsection{Énergie fournie par le rayonnement aux électrons}

    Il semble que,
    pour un rayonnement de fréquence $\nu$ fixée,
    si l'on augmente l'intensité lumineuse $I$
    alors
    l'énergie fournie à chaque électron
    va augmenter.

    Donc,
    on s'attends à tracer des courbes
    $\Delta_s(I)$
    qui vérifient~:
    $\pdv{\Delta_s}{I} > 0$.
    Mais
    les constatations expérimentales
    ne vont pas dans ce sens.
    On constate que
    $\Delta_s = T$ est indépendante de $I$
    ($\pdv{\Delta_s}{I} = 0$),
    mais que
    $\pdv{i}{I} > 0$~:
    augmenter $I$
    augmente le nombre d'électrons
    qui circulent.

    Finalement,
    modifier $I$
    ne change pas
    l'énergie cinétique $T$ des électrons arrachés\dots
    Par contre,
    des expériences menées
    à $\nu$ variable indiqueront que,
    $\pdv{\Delta_s}{\nu} > 0$,
    et on peut modéliser~:
    \begin{equation*}
        T
        = a \nu + \cst
    \end{equation*}

    Il semble que l'énergie cinétique
    des électrons libérés
    dépende
    de la fréquence
    du rayonnement incident
    plutôt que
    de son intensité.

    Nous avions
    par bilan d'énergie~:
    \begin{equation*}
        E_f
        = T + \cst
        \implies
        T
        = E_f + \cst
    \end{equation*}
    Et maintenant~:
    \begin{equation*}
        T
        = a \nu + \cst
    \end{equation*}

    Si l'on identifie~:
    \begin{equation*}
        E_f
        = a \nu
    \end{equation*}
    on obtient un moyen de déterminer
    l'énergie fournie
    par le rayonnement
    à chaque électron.
    Les données expérimentales
    indiquent
    $a = h$,
    avec $h = \SI{6.62e-34}{\joule\second}$
    qui prend la valeur
    de la constante de \propername{Planck}.

    Ce résultat,
    indépendant de l'intensité de la lumière
    et dépendant de la fréquence,
    met la physique classique en défaut.
    Il indique que
    l'échange d'énergie
    entre la lumière et un électron
    se fait avec une énergie fixée
    $h \nu$
    (remarquons que celà confirme notre hypothèse
    qui expliquait l'existance de la fréquence seuil $\nu_0$).

    La constante
    étant en parfait accord avec
    la constante de \propername{Planck},
    \propername{Einstein}
    raviva en 1905
    l'idée de \propername{Newton} selon laquelle
    la lumière serait consituée
    de particules d'énergie
    (qui furent appelées \emph{quantas})
    et que la matière pourrait
    les absorber.
    Mais le mécanisme d'échange
    n'était encore pas compris.

    N'oublions pas que
    plus l'intensité lumineuse est importante
    plus nombreux sont les échanges
    ($\pdv{i}{I} > 0$)~:
    le nombre de quantas augmente avec $I$.


\section{L'apparition du photon}

    \subsection{Diffusion \propername{Compton} (1923) \cite{dufour}
        \cite{dunodpcsi}}

    \begin{todo}
        Faire un schéma
        de l'expérience
        et des spectres.
    \end{todo}

    La diffusion \propername{Compton}
    est observée pour la première fois en 1923
    lors du bombardement de cibles en graphite
    par des rayons X.
    L'expérience montre que
    le spectre des rayons déviés par la matière
    comporte deux composantes~:
    l'une de fréquences égale
    à la fréquence du rayonnement incident
    et l'autre moins importante.

    \propername{Compton}
    interprète ces données
    comme résultats d'une collision élastique
    entre un quanta se comporant comme une particule ponctuelle
    et un électron de la cible de graphite.
    Le transfert d'énergie
    associé à la collision
    se traduit par une modification
    de la fréquence du rayonnement.

    Si l'on considère
    un quanta d'énergie $E_X$
    et d'impulsion $\vb{p}_X$
    qui collisionne un électron au repos ($\vb{p}_e = \vb{0}$)
    d'énegie de masse $m_e c^2$,
    les bilans
    d'énergie et d'impulsion
    vont s'écrire~:
    \begin{gather*}
        E_X + m_e c^2
        = E_X' + E_e'
        \\
        \vb{p}_X
        = \vb{p'}_X + \vb{p'}_e
    \end{gather*}
    Considérant les résulats
    de \propername{Planck}
    et d'\propername{Einstein}
    pour l'énergie
    d'un quanta associé à
    un rayonnement de fréquence $\nu$,
    on écrit~:
    \begin{equation*}
        h \nu + m_e c^2
        = h \nu' + E_e'
        \implies
        E_e'
        = m_e c^2 - h (\nu - \nu')
    \end{equation*}
    par ailleurs,
    \begin{align*}
        E_e'^2
        &= m_e^2 c^4 + \vb{p_e'}^2 c^2 \\
        &= m_e^2 c^4 + (\vb{p}_X - \vb{p'}_X)^2 c^2
    \end{align*}
    En écrivant
    $E_X = p_X c = h \nu$
    et
    $E_X' = p_X' c = h \nu'$
    puis en traitant les équations (\cite{dufour}),
    on pourra obtenir~:
    \begin{equation*}
        \nu - \nu'
        = \frac{h \nu \nu'}{m_e c^2} (1 - \cos\theta)
    \end{equation*}
    où $\theta$ est
    l'angle auquel le rayonnement dévié
    est mesuré.
    Ce résultat
    est annoncé sous l'hypothèse
    que la lumière peut-être traitée
    comme une particule
    lors de collisions.
    Cette particule
    porteuse d'énergie
    serait aussi
    porteuse d'une \emph{quantité de mouvement}.

    Les résultats surprenant de \propername{Compton}
    eurent d'abord du mal à être acceptés
    mais finirent par convaincre les scientifiques
    qu'un modèle corpusculaire
    pouvait \emph{aussi}
    être utilisé pour décrire la lumière.
    Ces particules furent appelées \emph{photons}.

    \subsection{Le photon \cite{dunodpcsi} \cite{fabre}}

    \subsubsection{Énergie}

    Comme nous l'avons vu,
    un photon porte l'énergie
    $\epsilon
    = h \nu
    = h \frac{c}{\lambda}$,
    avec $h = \SI{6.62e-34}{\joule\second}$
    et $c = \SI{3e8}{\meter\per\second}$.

    \begin{media}
        \url{http://people.whitman.edu/~dunnivfm/FAASICPMS_Ebook/CH1/Figures/F1_2_Electromagnetic_Spectrum.gif}
    \end{media}

    \subsubsection{Quantité de mouvement, masse, vitesse}

    Nous avons vu que
    l'énergie d'un photon est
    $E = h \nu$,
    sa quantité de mouvement est,
    par conséquent
    $\vb{p} = \flatfrac{h \nu}{c} \vb{u}$,
    ce que l'on écrit en relativité restreinte~:
    \begin{equation*}
        \vq{P}
        = \vqq{h \nu}
              {\frac{h \nu}{c}\vb{u}}
        \implies
        m_p^2 c^4
        = - E^2 + \vb{p}^2 c^2
        \implies
        m_p
        = 0
    \end{equation*}

    Puisqu'il est vecteur de la lumière
    il se déplace à la même vitesse
    ($c$ dans le vide et,
    $\flatfrac{c}{n}$ dans un milieu d'indice $n$).

    Pour ordre de grandeur,
    la quantité de mouvement
    portée par un photon du visible
    à $\lambda = \SI{500}{\nano\meter}$
    ($\nu = \SI{600e12}{\hertz}$)
    est d'environ
    $p = \SI{1.3e-27}{\kilogram\meter\per\second}$.
    Cette valeur est très faible
    mais il semblerait que
    avec une mole de photons ($N = \num{6e23}$)
    incidente chaque seconde,
    on obtiendrait une valeur de force
    $F_{mole}
    = \frac{p_{mole}}{\SI{1}{\second}}
    = \SI{8.0e-4}{\newton}$
    largement mesurable.

    \subsubsection{Nombre de photons \cite{dunodpcsi}, pression de radiation}

    \paragraph{Laser}
    Dans les expérience d'optiques
    il est rare que l'aspect corpusculaire de la lumière
    se manifeste.
    Estimons le nombre de photons
    émis chaque seconde
    par un laser hélium-néon
    ($\lambda = \SI{633}{\nano\meter}$,
    $\nu = \SI{473e12}{\hertz}$)
    de puissance $P = \SI{1,0}{\milli\watt}$.
    Pendant $\Delta T = \SI{1}{\second}$,
    le laser délivre une énergie
    $E = P \Delta T = \SI{1e-3}{\joule}$.
    L'énergie d'un photon est
    $\epsilon = h \nu = \SI{3.1e-19}{\joule}$
    alors le nombre de photons est~:
    \begin{equation*}
        N
        = \frac{E}{\epsilon}
        = \num{3.2e15}
    \end{equation*}

    Ce nombre est élevé
    et explique que l'on ne percoive pas
    l'aspect granulaire de la lumière.
    Il reste cependant
    trop faible devant le nombre d'\propername{Avogadro}
    ($\frac{N}{\glssymbol{Na}} = \num{5.3e-9}$)
    pour envisager une poussée détectable
    ($F_{laser} = \SI{e-13}{\newton}$).

    \paragraph{Voile solaire}
    Le flux énergétique solaire
    mesuré hors de l'atmosphère
    est lui de l'ordre
    $\phi
    = \SI{1000}{\watt\per\meter\squared}
    (= \SI{1}{\milli\watt\per\milli\meter\squared})$,
    l'énergie étant centrée autour
    du domaine de longueurs d'onde visibles,
    une estimation à
    $\lambda = \SI{500}{\nano\meter}$
    prévoit une \emph{pression} proche de
    $P = \SI{6e-6}{\newton\per\meter\squared}$.

    Pour accélérer une masse de $\SI{1}{\kilogram}$
    à $\SI{1}{\meter\per\second\squared}$,
    la surface utile
    sur laquelle les photons solaires
    seraient collectés
    devrait être de l'ordre de
    $\SI{150000}{\meter\squared}$,
    atteignable avec un carré de
    $\SI{400}{\meter}$ de côté.
    Ces valeurs
    semblent proche de l'irréel
    mais
    l'agence d'expolration aérospatiale japonaise
    à développé un démonstrateur de \emph{voile solaire}
    nommé IKAROS~:
    le satelite de $\SI{315}{\kilogram}$
    constitué d'une voile
    en polymère réfléchissant
    de $\SI{15}{\kilogram}$
    et de superficie $\SI{173}{\meter\squared}$
    (carrée de côté $\SI{14.1}{\metre}$,
    d'épaisseur $\SI{7.5}{\micro\metre}$)
    peut augmenter sa vitesse
    d'une dizaine de mètre par seconde chaque mois
    ($\SI{9.5}{\meter\per\second\per\month}$).

    \begin{media}
        Photo de IKAROS déployé en orbite,
        prise par la caméra ejectée~:\\
        \url{http://global.jaxa.jp/press/2010/06/img/20100616_ikaros_3.jpg}

        Trajectoire de IKAROS,
        qui utilise sa voile solaire
        pour ajuster son orbite
        jusqu'à croiser Vénus~:\\
        \url{http://www.isas.jaxa.jp/e/forefront/2011/tsuda/image/fig_03.gif}
    \end{media}

    \paragraph{Queue des comètes}
    Toujours dans l'espace,
    l'orientation de la queue de poussière des comètes
    s'explique par
    la pression de radiation
    exercée par le Soleil sur leur \emph{chevelure}.

    \begin{media}
        La comète périodique \propername{Encke}
        découverte depuis Paris en 1786.
        Ici imagée par la NASA en 2007,
        «~perdant~» sa queue~:\\
        \url{https://commons.wikimedia.org/wiki/File:Encke_tail_rip_of.gif}

        Diagramme d'une comète (image NASA)~:\\
        \url{https://commons.wikimedia.org/wiki/File:Comet_Parts.jpg}

        Position de la queue
        avant et après
        le périhélie \cite{demoulin}~:\\
        \url{http://www.astro.ulg.ac.be/~demoulin/queue.gif}
    \end{media}

\plusloin{
    On pourra parler
    d'une expérience plus moderne
    \cite{dunodpcsi} \cite{fabre}
    dans laquelle on éclaire
    avec une source de photons uniques
    une lame semi-réfléchissante.
    Des détecteurs
    recueillent les faisceaux transmis et réfléchis,
    on constate
    qu'ils fournissent des signaux de pics brefs
    qui ne sont jamais simultanés.

    On pourra réinsister sur la caractère
    \emph{dual} onde-corpscule
    de la lumière
    en rappelant les résultats
    de l'expérience des fentes de \propername{Young}
    à un photon.

    Le moment cinétique
    $L = \flatfrac{h \nu}{\omega} = \hbar$
    porté par les photons
    de lumière polarisée circulairement
    n'a pas été évoqué,
    mais on peut décrire
    l'expérience dans laquelle on mesure un couple
    exercé par la lumière
    sur une lame biréfreingente
    \cite{delannoy2004}.

    C'est suite à cette leçon
    que nous pourrons décrire les processus microscopiques
    d'interaction lumière-matière
    d'absorbtion et d'émission.
    En particulier,
    on reviendra vers la notion
    de quantité de mouvement des photons
    si l'on désire décrire les expériences
    de refroidissement d'atomes.
}
