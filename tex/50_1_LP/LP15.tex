\chapter{Transitions de phase}

\prereqstart
{Notions sur les changement d'états des corps purs}
{Potentiels thermodynamiques}
\prereqstop

\section{Évolution d'un corps pur lors d'un changement d'état}

    \subsection{Potentiels thermodynamiques}

    On considère ici
    un corps pur
    dans des conditions expérimentales où
    la pression $P$
    et la tempértaure $T$
    sont fixées.
    Nous avons déjà vu que
    lors de l'étude
    d'un tel système thermodynamique
    la fonction enthalpie libre externe $G^\ast$
    renseigne
    à la fois
    sur les conditions d'équilibre
    et le sens d'évolution.
    Rappelons alors
    qu'elle est définie par~:
    \begin{equation*}
        G^\ast
        \defeq F^\ast + P_0 V
        = U - T_0 S + P_0 V
    \end{equation*}
    et qu'elle varie selon~:
    \begin{equation*}
        \Delta G^\ast
        = \Delta(U - T_0 S + P_0 V)
        \leq 0
    \end{equation*}
    et que,
    pour des transformations où
    l'état initial
    et l'état final
    sont tous les deux
    des états d'équilibre thermique avec le thermostat,
    alors
    la fonction enthalpie libre~:
    \begin{equation*}
        G = U - TS + PV
    \end{equation*}
    joue \emph{aussi}
    un rôle de potentiel thermodynamique,
    en plus d'être fonction d'état.

    Comme notre système
    est diphasé
    il va être utile
    d'utiliser la propriété d'extensivité
    de l'enthalpie libre.
    On écrit alors
    $G_s$ l'enthalpie libre de la phase solide
    et
    $G_l$ l'enthalpie libre de la phase liquide,
    puis~:
    $G = G_s + G_l$.
    On introduit
    les grandeurs molaires
    avec des notations usuelles
    de sorte que~:
    \begin{equation*}
        G(T, P, n)
        = n_s g_s(T, P) + n_l g_l(T, P)
    \end{equation*}
    \begin{draft}
        Attention,
        cette décomposition n'est possible
        lorsqu'on néglige les effets d'interactions
        au niveau de l'interface.
        On peut le faire
        pour un système
        où les deux phases
        sont séparées en deux grand volumes
        mais pas
        pour des mousses.
        Les énergies
        associées à la surface
        peuvênt être liées
        à la tension de surface
        ou
        \dots
    \end{draft}

    \subsection{Évolution et équilibre}

    \subsubsection{Condition d'évolution}

    La condition d'évolution spontannée (ou d'équilibre)
    est donnée par~:
    \begin{align*}
        \dd G
        \leq 0
        &\implies
        g_s(T, P) \dd n_s + g_l(T, P) \dd n_l
        \leq 0
        \\
        &\implies
        \qty[g_s(T, P) - g_l(T, P)] \dd n_s
        \leq 0
    \end{align*}
    donc,
    \begin{itemize}
        \item si $g_s(T, P) > g_l(T, P)$
            alors $\dd n_s < 0$~:
            le liquide se solidifie~;
        \item si $g_s(T, P) < g_l(T, P)$
            alors $\dd n_s > 0$~:
            le solide fond~;
        \item éventuellement si $g_s(T, P) = g_l(T, P)$
            alors le système est à l'équilibre~:
            $T = T_{eq}$ et $P = P_{eq}$.
    \end{itemize}

    La notation des dépendances $(T, P)$
    est un peu loudre mais essentielle.
    La condition d'équilibre
    donnée par l'égalité des enthalpies libres molaires
    à $T_{eq}$ et $P_{eq}$ de l'expérience
    donne implicitement
    une relation $P_{eq} = \fof(T_{eq})$
    qui,
    lorsqu'elle est vérifiée,
    correspond à l'état d'équilibre~;
    réciproquement,
    dans l'état d'équilibre
    elle est vérifiée.

    \subsubsection{L'enthalpie libre molaire}

    Nous avons introduit
    deux nouvelles grandeurs thermodynamiques
    pour décrire chaque sous-système.
    La condition d'évolution (ou d'équilibre)
    suggère que
    pour connaître l'état du système complet
    à une température et une pression données
    il faut comparer $g_s(T, P)$ à $g_l(T, P)$,
    seulement,
    nous n'avons aucune idée
    des valeurs que prennent ces grandeurs.

    Leurs valeurs
    vont de toute façon
    être tabulées
    de sorte à ce qu'elles soient cohérentes
    avec l'étude que nous avons menée.
    Mais nous allons voir tout de suite
    que leurs dépendances
    à $T$ et $P$
    ne sont pas arbitraires.

    Pour la phase solide,
    les variations
    de l'enthalpie libre molaire $g_s$
    sont données par
    (on utilise
    les grandeurs molaires
    par extensivité)~:
    \begin{align*}
        \dd g_s
        &= \dd(u_s - T_0 s_s + P_0 v_s)
        \neq \dd u_s - T_0 \dd s_s + P_0 \dd v_s
        \\
        &= \dd u_s - s_s \dd T_0 - T_0 \dd s_s + P_0 \dd v_s + v_s \dd P_0
        \\
        &= \dd u_s - s_s \dd T - T \dd s_s + P \dd v_s + v_s \dd P
        \qq{(quasi-statique)}
        \\
        &= T \dd s_s - P \dd v_s - s_s \dd T - T \dd s_s + P \dd v_s + v_s \dd P
        \qq{(id thermo)}
        \\
        &= - s_s \dd T + v_s \dd P
    \end{align*}

    \paragraph{Variation avec $T_0$}
    On fait varier
    la température extérieure $T_0$,
    toute en assurant l'équilibre thermique $T = T_0$.
    Dans ce cas~:
    \begin{equation*}
        \pdv{g_s}{T}
        = -s_s
        \qq{et}
        \pdv{g_l}{T}
        = -s_l
    \end{equation*}

    L'entropie molaire est~:
    \begin{itemize}
        \item toujours positive,
        \item plus grande pour les liquides que les solides,
    \end{itemize}
    ce qui est résumé graphiquement ci-dessous.
    Comme le système
    évolue toujours
    vers l'état d'énergie libre minimale,
    la phase stable
    aux températures $T > T_{eq}$
    est la phase liquide
    (et inversement).

    \paragraph{Variation avec $P_0$}
    Le même calcul
    mais en faisant varier
    la pression extérieur $P_0$
    va donner~:
    \begin{equation*}
        \pdv{g_s}{P} = v_s
        \qq{et}
        \pdv{g_l}{P} = v_l
    \end{equation*}
    le volume molaire
    étant toujours positif,
    l'enthalpie libre molaire
    est croissante avec la pression.
    Le volume molaire
    de la phase liquide
    est \emph{en général} plus grand
    que celui de la phase solide,
    mais ça n'est pas le cas de l'eau.

    \begin{todo}
        Calculer
        les dérivées secondes
        pour justifier
        la concavité/convexité
        des courbes~!
    \end{todo}

    \begin{figure}[H]
        \centering
        \begin{tikzpicture}
            \begin{axis}[
                height = 5 cm,
                width = \linewidth/3,
                axis lines = left,
                ylabel = {$g$},
                ylabel style = {at=(current axis.above origin), rotate=-90},
                xtick={6.7},
                xticklabel={$T_{eq}$},
                xmin=0,
                xmax=11,
                ytick={2},
                ymin=-11,
                ymax=1,
            ]
            \addplot[mark=, dotted] coordinates{(6.7, 0) (6.7, -11)};

            \addplot[domain=1:6.7,color=blue,dashed]
                {-x^2/10};
            \addplot[domain=6.7:10,color=blue]
                {-x^2/10} node[below,pos=.5] {$g_l$};

            \addplot[domain=1:6.7,color=red]
                {-x^1.7/10-2} node[below,pos=.5] {$g_s$};
            \addplot[domain=6.7:10,color=red,dashed]
                {-x^1.7/10-2};
            \end{axis}
        \end{tikzpicture}
        \begin{tikzpicture}
            \begin{axis}[
                height = 5 cm,
                width = \linewidth/3,
                axis lines = left,
                ylabel = {$g$},
                ylabel style = {at=(current axis.above origin), rotate=-90},
                xtick={6.7},
                xticklabel={$P_{eq}$},
                xmin=0,
                xmax=11,
                ytick={-2},
                ymin=-1,
                ymax=11,
            ]
            \addplot[mark=, dotted] coordinates{(6.7, -1) (6.7, 11)};

            \addplot[domain=1:6.7,color=blue,dashed]
                {x^2/10};
            \addplot[domain=6.7:10,color=blue]
                {x^2/10} node[above,pos=.5] {$g_l$};

            \addplot[domain=1:6.7,color=red]
                {x^1.7/10+2} node[above,pos=.5] {$g_s$};
            \addplot[domain=6.7:10,color=red,dashed]
                {x^1.7/10+2};
            \end{axis}
        \end{tikzpicture}
        \begin{tikzpicture}
            \begin{axis}[
                height = 5 cm,
                width = \linewidth/3,
                axis lines = left,
                ylabel = {$g$},
                ylabel style = {at=(current axis.above origin), rotate=-90},
                xtick={6.7},
                xticklabel={$P_{eq}$},
                xmin=0,
                xmax=11,
                ytick={-2},
                ymin=-1,
                ymax=11,
            ]
            \addplot[mark=, dotted] coordinates{(6.7, -1) (6.7, 11)};

            \addplot[domain=1:6.7,color=red,dashed]
                {x^2/10};
            \addplot[domain=6.7:10,color=red]
                {x^2/10} node[above,pos=.5] {$g_s$};

            \addplot[domain=1:6.7,color=blue]
                {x^1.7/10+2} node[above,pos=.5] {$g_l$};
            \addplot[domain=6.7:10,color=blue,dashed]
                {x^1.7/10+2};
            \end{axis}
        \end{tikzpicture}
    \end{figure}

    \subsubsection{Le diagramme de phases}

    Les graphes précédents
    permettent de déterminer
    laquelle des phases est stable
    à haute (ou basse) température
    ou
    à haute (ou basse) pression.
    Cela permet d'établir
    une ébauche du diagramme (P, T) de phases,
    mais nous pouvons facilement
    calculer la pente de la courbe d'équilibre.
    En effet,
    nous avons vu que
    l'équilibre est donné par
    $g_s(T_{eq}, P_{eq}) = g_l(T_{eq}, P_{eq})$
    alors si l'on dérive
    par rapport à la température
    en restant sur la courbe~:
    \begin{align*}
        g_s(T_{eq}, P_{eq})
        &= g_l(T_{eq}, P_{eq})
        \qq{avec}
        P_{eq} = \fof(T_{eq})
        \\
        \implies
        \dv{g_s}{T_{eq}}
        &= \dv{g_l}{T_{eq}}
        \\
        \implies
        \pdv{g_s}{T_{eq}} + \pdv{g_s}{P_{eq}} \pdv{P_{eq}}{T_{eq}}
        &= \pdv{g_l}{T_{eq}} + \pdv{g_l}{P_{eq}} \pdv{P_{eq}}{T_{eq}}
    \end{align*}
    en remplacant
    les dérivées partielles
    par les expressions trouvées plus haut,
    on exprime la pente de la courbe d'équlibre~:
    \begin{equation*}
        -s_s + v_s \pdv{P_{eq}}{T_{eq}}
        = -s_l + v_l \pdv{P_{eq}}{T_{eq}}
        \implies
        \pdv{P_{eq}}{T_{eq}}
        = \frac{s_l - s_s}{v_l - v_s}
        = \frac{\abs{s_l - s_s}}{v_l - v_s}
    \end{equation*}
    c'est la relation de \propername{Clapeyron}.
    On retrouve que
    la pente de la courbe d'équilibre
    dépend du signe de $v_s - v_l$
    qui est \emph{en général} négatif,
    mais pas pour l'eau.

    \subsection{Enthalpie de changement d'état}

    \begin{draft}
        Attention~:
        contraîrement à ce qui est annoncé ci dessous,
        l'équilibre n'est pas vérifié
        pour les changement d'états
        d'un équilibre métastable
        vers un équlibre stable
        (mais ne pas en parler,
        pour le pas compliquer).
        Dans le cas
        d'un liquide surfondu
        (glaçe à $T > \SI{0}{\degreeCelsius}$)
        on aurait
        $g_l < g_s$
        et pas l'égalité.
    \end{draft}
    Tout au long
    du changement d'état
    l'équilibre est vérifié
    et $\dd G = 0$.
    Mais
    pour que le changment d'état ait lieu,
    il faut apporter
    ou extraire
    de l'énergie au système,
    considérons
    une liquéfaction
    (gaz vers liquide)
    pendant laquelle
    on extrait
    de l'énergie au système.
    Pour nous la fournir
    le système en puise
    dans l'agitation thermique
    qui assure la «~mauvaise~» cohésion du milieu,
    c'est cela qui entraîne
    la variation d'entropie.
    Bien que
    l'enthalpie libre reste constante,
    l'enthalpie varie~:
    \begin{equation*}
        \dd H
        = \dd G + \dd(TS)
        = T \dd S
    \end{equation*}

    En somme,
    lorsque
    le changement d'état
    est réalisé complètement~:
    \begin{equation*}
        \Delta_{\textrm{liq}} H
        = T \Delta_{\textrm{liq}} S
        = T n (s_l - s_v)
        = - T n \abs{s_v - s_l}
        < 0
    \end{equation*}
    ce changement d'état
    est endothermique,
    et on appelle
    \emph{enthalpie molaire de vaporisation}
    la quantité positive
    $l_{\textrm{vap}} = T \abs{s_v - s_l}$.
    On écrit alors,
    pour la solidification~:
    \begin{equation*}
        \Delta_{\textrm{liq}} H
        = - n l_{\textrm{vap}}
    \end{equation*}
    L'enthalpie molaire de liquéfaction
    nous renseigne sur
    la quantité d'énergie
    par unité de quantité de matière
    que l'on doit extraire du système
    pour transformer
    le liquide en gaz.

    On peut s'intéreser
    à des ordres de grandeur
    et les comparer
    à des capacités thermiques molaires
    et remarquer
    que les changement d'états
    mettent en jeu
    beaucoup d'énergie~:
    \begin{table}[H]
        \centering
        \begin{tabular}{rSSSSS}
            \toprule
            Substance
            & {$M$ (\si{\gram\per\mole})}
            & {T (\si{\degreeCelsius})}
            & {$c_{p,l}$ (\si{\joule\per\kilogram\per\kelvin})}
            & {$l_{\textrm{vap}}$ (\si{\joule\per\kilogram})}
            & {$c_{p,g}$ (\si{\joule\per\kilogram\per\kelvin})}
            \\
            \midrule
            Eau
            & 18
            & 100
            & 4216
            & 2257e3
            & 2078
            \\
            Ethanol
            & 115
            & 79
            & 5159
            & 855e3
            & 2899
            \\
            \bottomrule
        \end{tabular}
    \end{table}

    On définit aussi
    la grandeur associée,
    \emph{entropie molaire de solidification}~:
    \begin{equation*}
        \Delta_{\textrm{liq}} h
        = h_l - h_v
        = T (s_l - s_v)
        \implies
        \Delta_{\textrm{liq}} s
        = \frac{\Delta_{\textrm{liq}} h}{T}
    \end{equation*}

    \begin{draft}
        L'enthalpie molaire de changement d'état
        $l$ dépend
        de la température $T_{eq}$
        \emph{ou}
        de la pression $P_{eq} = \fof(T_{eq})$.
    \end{draft}

    \begin{todo}
        Donner des ordres de grandeur.
        Donner un graphe
        $l_{\textrm{vap}}(T)$~?
    \end{todo}

    \begin{todo}
        On peut reprendre
        la formule de \propername{Clapeyron}~:
        \begin{equation*}
            \pdv{P_{eq}}{T_{eq}}
            = \Delta h / T \Delta v
            = l /T \Delta v
        \end{equation*}
        Près du point critique
        (courbe liquide gaz)
        on dirait que
        $\pdv*{P_{eq}}{T_{eq}}$
        tends vers l'infini
        car les volumes molaires
        sont de plus en plus proches.
        Mais il se trouve
        que les entropies molaires
        et les enthalpies molaires
        le sont aussi.
        Donc
        il n'y a pas
        de discontinuité,
        mais
        la courbe d'équilibre liquide gaz
        cesse d'être définie,
        et l'enthalpie de changement d'état
        est nulle.
    \end{todo}

    \subsection{Deux types de transitions}

    \begin{todo}
        Les changements d'état
        sont du premier ordre,
        la transition vers le fluide supercritique
        est du deuxième ordre.
    \end{todo}

    \begin{media}
        Une expérience intéressante
        sur la transition supercritique~:
        \url{https://www.youtube.com/watch?v=jMfDBOg8ibY}
    \end{media}

\section{Une autre transition}
