\chapter{Contact entre deux solides, frottement}

\cite{perezmecanique}

  \section{Cinématique}

    \subsection{Vitesse de glissement}

    \paragraph{Translation pure} La vitesse de glissement $\vb{v_g}$
    entre deux solides $S_1$ et $S_2$
    en contact et en translation l'un par rapport à l'autre
    est la vitesse relative entre deux points $I_1$ et $I_2$
    (appartenant respectivement à $S_1$ et $S_2$)
    situés au niveau de la surface de contact~:
    \begin{equation*}
        \vb{v_g}
        = \vb{v}_{I_1/I_2}
    \end{equation*}
    Si $\vb{v_g} = \vb{0}$
    il y a \emph{adhérence} entre les deux solides,
    sinon il y a \emph{glissement}.

    \paragraph{Translation et rotation}

    On considère une roue ($S_1$)
    de centre $O'$
    qui se déplace sur sol avec le vecteur rotation $\vb{\omega}$
    et la vitesse linéaire $\vb{v}_{O'/O}$.
    La vitesse d'un point $M$ sur la roue par rapport à $O$ s'écrit~:
    \begin{equation*}
        \vb{v}_{M/O}
        = \vb{v}_{M/O'} + \vb{v}_{O'/O}
        = \vb{\omega}\ \va{O'M} + \vb{v}_{O'/O}
    \end{equation*}
    On peut exprimer cette vitesse pour le point $I_1$ lié à la roue
    qui se situe au niveau du contact entre la roue et le sol~:
    \begin{equation*}
        \vb{v}_{I_1/O}
        = \vb{v}_{O'/O} + \vb{\omega}\ \va{O'I_1}
    \end{equation*}
    Pour exprimer la vitesse de glissement de la roue sur le sol,
    on doit  introduire un point $I_2$ lié au sol
    et situé au niveau du contact entre la roue et le sol~:
    \begin{equation*}
        \vb{v_g}
        = \vb{v}_{I_1/I_2}
        = \vb{v}_{I_1/O} - \vb{v}_{I_2/O}
        = (\vb{v}_{O'/O} + \vb{\omega}\ \va{O'I_1}) - (\vb{0})
        = \vb{v}_{O'/O} + \vb{\omega}\ \va{O'I_1}
    \end{equation*}
    Il y a donc adhérence entre la roue et le sol si et seulement si
    $\vb{v}_{O'/O} = \vb{\omega}\ \va{O'I_1}$\,
    dans le cas contraîre il y a glissement.

    \begin{manip}
        Illustrer, par exemple avec
        une roue bloquée qui avance puis
        une roue qui tourne sur place.
    \end{manip}

    \subsection{Centre instantané de rotation}

    Pour un roulement sans glissement on a~:
    \begin{equation*}
        \vb{v}_{I_1/O}
        = \vb{v}_{O'/O} + \vb{v}_{I_1/O'}
        = - \vb{\omega} * \va{I_1O} + \vb{\omega} * \va{I_1O}
        = \vb{0}
    \end{equation*}
    À l'instant précis représenté,
    le point $I_1$ de la roue a une vitesse nulle par rapport au sol.
    On l'appelle \emph{centre instantané de rotation}.

  \section{Dynamique}

    \subsection{Forces mises en jeu}

    \begin{manip}
        Montrer la manip,
        montrer que le palet ne glisse pas
        même si le dynamomètre tire dessus.
    \end{manip}

    On étudie un palet ($S_1$)
    tracté par un dynamomètre
    qui exerce une force $\vb{F}$ parallèle au support horizontal.
    Ce dernier
    exerce sur la palet une force $\vb{R}$.

    Si $S_1$ est immobile,
    le principe fondamental de la dynamique
    assure l'équilibre des forces~:
    \begin{equation*}
        \vb{F} + \vb{P} + \vb{R} = \vb{0}\
        \implies
        \vb{R} = - (\vb{F} + \vb{P})
    \end{equation*}
    Il sera utile de
    décomposer $\vb{R}$ comme la somme
    d'une force verticale $\vb{N}$ (normale au support),
    et d'une force horizontale $\vb{T}$ (tangantielle au support)
    et dite \emph{force de frottements},
    l'intéret étant de pouvoir opposer les quatre forces
    ($\vb{P}$, $\vb{F}$, $\vb{N}$, $\vb{T}$)
    appliquées sur le solide $S_1$
    deux à deux.

    D'une part
    le dynamomètre permet de mesurer $\norm{\vb{F}}$
    et donc de déterminer $\norm{\vb{T}} = - \norm{\vb{F}}$,
    d'autre part l'équilibre mécanique nous permet de connaître
    $\norm{\vb{N}} = - \norm{\vb{P}}$.

    \subsection{Lois de \propername{Cloulomb} (1785)}

    \begin{todo}
        Qu'on fait Amonton 1699 et DeVinci 1508 ?
    \end{todo}

    \begin{manip}
        Montrer la mesure,
        puis montrer
        ce que l'on observe sur le dynamomètre
        lorsque le palet commence à glisser.
    \end{manip}

    On constate que~:
    \begin{itemize}
        \item $S_1$ adhère tant que $\norm{\vb{F}} <
            \norm{\vb{F_{lim}}}$,
        \item Si $S_1$ est en mouvement rectiligne uniforme alors
            $\norm{\vb{F}} = \cst = \norm{\vb{F_{dyn}}}$.
    \end{itemize}

    Donc quand $S_1$ est immobile,
    la force de frottements $\vb{T}$
    s'adapte à $\vb{F}$ pour laisser $S_1$ en adhérence.
    Mais au delà d'un certain seuil en norme $\norm{\vb{T_{lim}}}$,
    $S_1$ commence à glisser.

    Quand $S_1$ est en MRU,
    la force de frottements vaut en norme $\norm{\vb{T}} = \norm{\vb{T_{dyn}}}$
    et est dans le sens opposé au mouvement.
    Des manips plus avancées permettent de montrer
    que l'égalité est valable quelque soit le mouvement.

    Les expériences montrent plus quantitativement que~:
    \begin{equation*}
        \norm{\vb{T_{lim}}} = \mu_s \norm{\vb{N}}
    \end{equation*}
    \begin{equation*}
        \norm{\vb{T_{dyn}}} = \mu_d \norm{\vb{N}}
    \end{equation*}
    Où $\mu_s$ et $\mu_d$
    sont les coefficients de frottements
    \emph{statique}
    et \emph{dynamique}
    qui dépendent
    des matériaux constituants les surfaces en contact.

    \begin{todo}
        Exemples de valeurs
    \end{todo}

    On a parlé de $\vb{T}$
    mais alors que peut on dire sur $\vb{R}$ ?

    Comme $\norm{\vb{N}}$ ne change pas,
    on peut définir un angle $\alpha$
    entre $\vb{N}$ et $\vb{R}$
    qui dépend seulement de $\norm{\vb{T}}$.
    Pour $\norm{\vb{T}} = \norm{\vb{T_{lim}}}$,
    on a $\alpha = \alpha_{lim}$.
    Le solide reste donc immobile
    tant que $\alpha < \alpha_{lim}$.
    Le principe fondamental de la dynamique
    permet d'obtenir~:
    \begin{equation*}
        \alpha_{lim}
        = \arctan\left({\frac{\norm{\vb{T_{lim}}}}{\norm{\vb{N}}}}\right)
        = \arctan\left({\mu_s}\right)
    \end{equation*}
    On définit alors
    le \emph{cône de frottements}
    de demi-angle d'ouverture $\Phi_s = \arctan\left(\mu_s\right)$.
    Les situations où
    $\norm{\vb{T}} < \norm{\vb{T_{lim}}}$
    sont équivalentes
    aux situations où
    $\vb{R}$ est orienté dans le cône.
    L'introduction de cet élément géométrique permet
    d'apréhender plus facilement les problèmes.

    On pourra aussi défnir
    $\Phi_d = \arctan\left(\mu_d\right)$
    demi-angle d'ouverture d'un autre cône
    sur lequel s'aligne la force $\vb{T_{dyn}}$
    lors du glissement.

    \paragraph{Influence de la surface de contact}

    Tout cela semble conforme à l'expérience,
    mais quelque chose est surprenant~:
    la surface de contact n'intervient nulle part
    or une surface plus grande
    semble prescrire plus de frottements
    (une adhérence meilleure).

    \begin{manip}
        Faire la manip avec
        une boîte parallélépipédique
        ou plusieurs palets
        de même constitution
        mais de surfaces différentes.
    \end{manip}

    On constate que
    la surface n'a pas d'influence sur $\norm{\vb{T_{lim}}}$
    ni sur $\norm{\vb{T_{dyn}}}$.

    \begin{pydo}
        Simulation python ?
    \end{pydo}

  \section{Exemples}

    \subsection{Bicyclette~: retour sur le roulement}

    \begin{draft}
    On considère un vélo
    immobile au départ de l'expérience~:
    $\vb{v_g} = \vb{0}$.
    Le poids de la roue
    et la force normale du sol sur la roue
    s'opposent.

    L'adhérence entre la roue et la route
    est à l'origine d'une force tangentielle au sol
    exercée par la roue sur la route
    lorsque que le cycliste appuie sur une pédale.

    Pour ne pas «~patiner~» en démarant,
    le cycliste doit exercer sur les pédales un couple
    tel que la force de réaction exercée par la route sur la roue
    soit orientée dans le cône de frottements.

    Si le vélo avance
    et que la roue ne glisse pas,
    c'est que~:
    \begin{itemize}
        \item La vitesse de glissement $\vb{v_g}$ est nulle,
        \item La résultante des forces exercées sur la bicyclette est orientée
            vers l'avant.
    \end{itemize}

    exerce une force sur la roue.
    Attention au schéma dangereux
    (deux forces s'opposent mais ne s'appliquent pas sur le même solide).

    La force $R$ du sol sur la roue
    est vers l'avant,
    tant qu'elle reste dans le cône,
    le vélo avance mais ne glisse pas.
    Si R sort du cône, on passe en glissement.

    Exp : faire du vélo sur la glace,
    on pédale "dans le vide".
    \end{draft}
