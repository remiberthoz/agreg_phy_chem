\chapter{Propagation avec dispersion}

Dans les leçons sur les ondes,
nous travaillons souvent avec
un outil mathématique très utile~:
l'onde plane progressive monochromatique
(on utilise $\z{X}(z, t) = x_0 e^{i(\omega t - k z + \phi)}$),
dont la propagation
est décrite par
l'équation de \propername{D'Alembert}.

L'expérience a montré que
les vitesses de propagation
d'ondes planes de pulsations différentes
sont elles aussi différentes.
Dès lors,
il semble compliqué
de modéliser la propagation
d'un \emph{signal}
avec l'équation de \propername{D'Alembert}
et les OPPM.

Dans cette leçon
nous allons donc étudier
la propagation de signaux composés~:
nous établirons
des relations de dispersions plus générales
que celles déjà connues
(dans différents domaines de la physique),
nous introduirons formellement
le concept de \emph{paquet d'onde}
déjà évoqué,
ainsi que les grandeurs appelées
\emph{vitesse de groupe}
et \emph{vitesse de phase}.

Ces notions présenteront des intérêts
en communication,
en mécanique quantique,
et très généralement
en modélisation de phénomènes physiques.

\section{Une nouvelle équation de propagation}

    \subsection{Nécessité et origine de la nouvelle équation}

    \begin{draft}
        \paragraph{Nécessité}
        Signal ne dure pas depuis toujours~:
        TF
        = signal composé
        = non harmonique~;
        + expérience
        (= dispersion)
        => \propername{D'Alembert} à généraliser

        \paragraph{Origine}
        On pourrait étudier
        un cas très abstrait
        où $c = \fof(\omega)$
        directement dans \propername{D'Alembert}
        mais,
        en général,
        on peut modéliser le milieu
        et obtenir
        une équation différentielle
        meilleure que \propername{D'Alembert}.
    \end{draft}

    \paragraph{Exemple~: ondes sur une corde vibrante}
    \cite{dunodpsi}
    Sur la corde vibrante,
    l'équation de propagation
    obtenue,
    lorsque l'on prend en compte
    des frottements fluides avec l'air
    proportionnels à la vitesse,
    est~:
    \begin{equation*}
        \pdv[2]{y}{x} - \frac{\mu}{T_0} \pdv[2]{y}{t}
        = \beta \pdv{y}{t}
    \end{equation*}
    où
    $\mu$ est la masse linéique,
    $T_0$ la tension dans la corde,
    $\beta$ un coefficient de frottement
    ($\si{\second\per\meter\squared}
    = \si{(\kilogram\per\meter\per\second)\per\newton}$).

    Parfois,
    lorsque les vitesses sont grandes,
    on modélise les frottements
    par une force $\propto \qty(\pdv{y}{t})^2$.
    On peut aussi faire ce choix ici,
    mais nous allons voir
    qu'il rendra la situation
    plus compliquée.

    \subsection{Équation linéaire, décomposition harmonique}

    On considère des ondes quelconques
    dans un milieu quelconque,
    régit par une équation différentielle du type~:
    \begin{equation*}
        \pdv[2]{s}{x} - \frac{1}{c^2} \pdv[2]{s}{t} = f(s)
    \end{equation*}
    Le premier membre
    correspond
    à ce que l'on connait déjà,
    le second membre
    va nous permettre de généraliser.
    L'une des propriétés très utile
    utilisée jusqu'ici
    dans l'étude des ondes
    des équations différentielles
    est que,
    lorsque l'équation est linéaire
    les combinaisons linéaires de solutions
    sont aussi solution.
    Cela,
    associé aux travaux de \propername{Fourier}
    sur les ondes,
    nous permettait d'étudier
    une seule onde plane progressive monochromatique.

    Si la fonction $f(s)$
    est linéaire,
    le raisonnement reste valide~:
    on peut étudier
    la propagation d'une seule composante,
    généraliser le résultat aux autres,
    et utiliser la transformée de \propername{Fourier}
    pour étudier
    la propagation d'un signal.
    C'est pourquoi,
    sur la corde vibrante,
    il sera plus aisé
    d'étudier les cas
    où l'on considère des frottements
    $\propto \pdv{y}{t}$.

    Dans d'autre domaines de la physique,
    l'étude du milieu de propagation
    permet très souvent
    d'écrire une relation linéaire,
    dans le cadre de certaines approximations,
    quelque exemples
    sont regroupés dans le tableau ci-dessous.
    On y trouvera aussi
    l'équation différentielle
    pour l'OPPM
    en notation complexe,
    choisie avec
    la convention pour les signes~:
    $\z{s}(x, t) = s_0 e^{i(\omega t - k x)}$.

    \begin{table}[H]
        \centering
        \begin{tabular}{>{\centering\arraybackslash}m{.2\linewidth}|c|c}
            \toprule
            Domaine
                & Équation différentielle
                    & Représentation harmonique \\
            \midrule
            Corde vibrante avec frottements $y(x, t)$
            \cite{dunodpsi}
                &
                \(\displaystyle
                    \pdv[2]{y}{x} - \frac{\mu}{T_0} \pdv[2]{y}{t}
                    = \beta \pdv{y}{t}
                \)
                &
                \(\displaystyle
                    k^2 \z{y} - \frac{\mu}{T_0} \omega^2 \z{y}
                    = - i \omega \beta \z{y}
                \)
                \\
            Plasma $\vb{E}(x, t)$
            \cite{lyceenaval}
                &
                \(\displaystyle
                    \pdv[2]{\vb{E}}{x} - \frac{1}{c^2} \pdv[2]{\vb{E}}{t}
                    = \mu_0 \sigma \pdv{\vb{E}}{t}
                \)
                &
                \(\displaystyle
                    k^2 \z{\vb{E}} - \frac{1}{c^2} \omega^2 \z{\vb{E}}
                    = - \frac{\omega_p^2}{c^2} \z{\vb{E}}
                \)
                \\
            Câble coaxial avec pertes $u(x, t)$
            \cite{dunodpsi}
                &
                \(\displaystyle
                    \pdv[2]{u}{x} - \frac{1}{c^2} \pdv[2]{u}{t}
                    = (r\Gamma + \Lambda g) \pdv{u}{t}
                \)
                &
                \(\displaystyle
                    k^2 \z{u} - \frac{1}{c^2} \omega^2 \z{u}
                    = - i \omega (r\Gamma + \Lambda g) \z{u}
                \)
                \\
            \bottomrule
        \end{tabular}
    \end{table}

    En reprenant la corde vibrante,
    nous pouvons obtenir
    une relation entre
    $k$ et $\omega$~:
    \begin{equation*}
        k^2
        = \frac{\mu}{T_0} \omega^2 - i \omega \beta
        \implies
        \z{k}(\omega)
        = k'(\omega) - i k''(\omega)
    \end{equation*}
    Cette relation
    est la
    \emph{relation de dispersion}
    que nous allons interpréter.

    \subsection{Relation de dispersion}

    Pour la propagation
    d'une onde
    dans un milieu
    où la relation de dispersion
    est connue,
    on écrit
    pour l'onde plane monochromatique~:
    \begin{equation*}
        \z{s}(x, t)
        = s_0 e^{i(\omega t - \z{k} x)}
        = s_0 e^{i(\omega t - k'x)} e^{-k'' x}
    \end{equation*}
    On lit dans cette écriture,
    que la partie réelle $k'$
    de $k$
    va jouer un rôle similaire
    à celui déjà connu,
    tandis que
    la partie imaginaire $k''$
    va modifier
    l'amplitude de l'onde
    au cours de sa propagation,
    plus précisément
    elle va
    (en général, sauf dans certains milieux actifs)
    l'atténuer
    avec $k'' > 0$,
    sur une distance caractéristique
    $\delta = \frac{1}{\abs{k''}}$.

    La vitesse de propagation
    de cette onde plane monochromatique
    est déterminée
    en suivant un plan d'onde
    entre deux instants
    (on fait abstraction de l'atténuation)~:
    \begin{align*}
        \z{s}(x, t)
        = \z{s}(x + \dd x, t + \dd t)
        &\implies
        \omega t - k'(\omega) x
        = \omega (t + \dd t) - k'(\omega) (x + \dd x)
        \\
        &\implies
        0
        = \omega \dd t - k'(\omega) \dd x
        \\
        &\implies
        v_\phi
        \defeq \dv{x}{t}
        = \frac{\omega}{k'(\omega)}
    \end{align*}
    On l'a vu,
    $k'(\omega)$
    n'est pas forcément
    une relation de proportionalité.
    Cette vitesse,
    dite \emph{vitesse de phase}
    va donc dépendre
    de la fréquence de l'OPPM
    dans les milieux dispersifs
    (où $k' \neq \flatfrac{\omega}{c}$).

    Pour la corde,
    si l'on supprime les frottements,
    on retrouve bien
    la relation de dispersion
    $k' = \frac{\omega}{\flatfrac{T_0}{\mu}}$
    et donc
    une vitesse de phase
    $\flatfrac{T_0}{\mu}$
    indépendante de la fréquence,
    et $k'' = 0$.

    Le plasma
    est un cas d'étude
    intéressant
    car la relation de dispersion~:
    \begin{equation*}
        k^2 = \frac{\omega^2 - \omega_p^2}{c}
        \implies
        k = \frac{1}{c} \sqrt{\omega^2 - \omega_p^2}
    \end{equation*}
    va permettre
    de distinguer aisément
    deux domaines de fréquences.
    La pulsation $\omega_p$
    appelée pulsation plasma~;
    pour $\omega > \omega_p$,
    $k = k' \in \mathbb{R}$
    tandis que
    pour $\omega < \omega_p$,
    $k = i k'' \in i \mathbb{R}$.
    Le premier cas
    correspond à un milieu
    dispersif
    non absorbant,
    le second cas
    correspond à un milieu absorbant
    (pas de propagation).
    On étudie donc,
    dans la suite,
    la propagation d'ondes électromagnétiques
    dans un plasma,
    la vitesse de phase étant donnée par~:
    $v_\phi(\omega)
    = \frac{c}{\sqrt{1 - \flatfrac{\omega_p^2}{\omega^2}}}
    > c$.

    La propagation d'ondes électromagnétiques
    dans un plasma
    peut sembler très théorique,
    mais notons
    que l'une des couches atomsphérique
    (l'ionosphère $\SIrange{90}{120}{\kilo\meter}$,
    voir Wikipédia pour plus de détails)
    est constitué de gaz peu dense,
    en permanence ionisé par le rayonnement solaire.
    L'ionosphère constitue alors
    un bon exemple de plasma.
    La communication
    avec des satellites
    nécessite de traverser cette couche atmosphérique.

    On a noté
    en toute légerté~:
    $v_\phi > c$,
    ce résultat doit interpeler.
    Dès l'introduction à cette leçon,
    nous discutions
    le problème que posent
    les ondes planes progressives monochromatiques~:
    dans des milieux dispersifs
    elles ne se propagent pas toutes
    à la même vitesse,
    et ne sont mathématiquement correctes
    que pour des étendues
    temporelle
    et spatiale
    infinies,
    elle restent malgré cela
    de bons outils de calculs.
    Lorsque l'on veut envoyer un \emph{signal}
    on emmet une onde finie,
    alors nécessairement
    composée de plusieurs fréquences
    (sa transformée de \propername{Fourier}
    n'est pas un pic de \propername{Dirac},
    ça n'est pas une OPPM).
    Nous allons maintenant étudier
    la propagation d'un signal réel
    pour voir si lui aussi
    se propage avec une vitesse
    supérieure à $c$.

\section{Propagation de paquets d'ondes}

    \subsection{Signaux réels, mais simples}

    Plutôt qu'une OPPM,
    on veut un «~bip~»
    si possible simple à exprimer
    en terme d'onde.

    Nous avons déjà mentioné
    que la linéarité
    de l'équation de propagation
    nous permet d'utiliser
    des combinaisons linéaires de solutions
    pour en trouver d'autres.
    Les OPPM sont des solutions,
    nous alons alors
    créer une autre solution $\z{s}(x, t)$~:
    \begin{equation*}
        \z{s}(x, t)
        = \int_{-\infty}^{+\infty}
            s_0(\omega)
            e^{i(\omega t - k(\omega) x)}
            \dd \omega
    \end{equation*}
    choisisons $s_0$
    assez simple,
    du type
    $s_0(\omega) = e^{\flatfrac{(\omega - \omega_0)^2}{2 \sigma^2}}$.

    \subsection{Évolution \cite{jackson}}

    Pour $\sigma \ll \omega_0$,
    nous dirons que
    seul les composantes $\omega$
    proches de $\omega_0$
    ont de l'importance.
    Nous pourront étudier
    la relation de dispersion
    au premier ordre~:
    \begin{equation*}
        k(\omega)
        \approx
        k_0 + (\omega - \omega_0) \eval{\pdv{k}{\omega}}_{\omega_0}
        \qq{avec}
        k_0 = k(\omega_0)
    \end{equation*}
    Alors en calculant
    l'amplitude $\z{s}(x, t)$~:
    \begin{align*}
        \z{s}(x, t)
        &= \int s_0(\omega) e^{i \qty(
            \omega t
            - k_0 x
            - (\omega - \omega_0) \eval{\dv{k}{\omega}}_{\omega_0} x
        )} \dd \omega
        \\
        &= e^{i \qty(- k_0 x + \omega_0 \eval{\dv{k}{\omega}}_{\omega_0} x)}
            \int s_0(\omega)
            e^{i \qty(\omega t - \omega \eval{\dv{k}{\omega}}_{\omega_0})}
             \dd \omega
        \\
        &= e^{i \qty(- k_0 x + \omega_0 \eval{\dv{k}{\omega}}_{\omega_0} x)}
            \int s_0(\omega)
            e^{i \omega \qty(t - \eval{\dv{k}{\omega}}_{\omega_0})}
             \dd \omega
        \\
        &= e^{i \qty(- k_0 x + \omega_0 \eval{\dv{k}{\omega}}_{\omega_0} x)}
            \int s_0(\omega)
            e^{i \omega t'}
             \dd \omega
    \end{align*}
    en posant
    $t' = t - \eval{\dv{k}{\omega}}_{\omega_0}$,
    on reconnait alors~:
    \begin{equation*}
        \z{s}(x, t)
        = e^{i \qty(- k_0 x + \omega_0 \eval{\dv{k}{\omega}}_{\omega_0} x)}
            \z{s}(0, t')
    \end{equation*}
    Le signal $s$
    en $x$
    à l'instant $t$
    correspond
    au signal
    qui était en $x=0$
    à l'instant $t' = t - \frac{1}{v_g} x$,
    avec
    $v_g = \dv{\omega}{k}$
    sa \emph{vitesse de groupe}.
    On peut comprendre
    graphiquement,
    en traçant $s(x, t)$
    à plusieurs instants successifs.
    \pyimgen{paquet_donde}

    \begin{todo}
        Discuter du prix Nobel 2018~!
    \end{todo}
    \begin{todo}
        Solitons ?
    \end{todo}
