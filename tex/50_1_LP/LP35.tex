\chapter{Diffraction de \propername{Fraunhofer}}

\begin{media}
    Illustration de
    la diffraction de la lumière
    par la Lune
    lors d'occultations,
    on y voit
    que l'intensité lumineuse mesurée au sol
    augmente et diminue
    avant de s'éteindre,
    comme le prévoit la théorie~:
    \url{https://www.youtube.com/watch?v=WDFmRlZyAf8}
\end{media}

\begin{manip}
    Mettre en place
    la manip
    rigoureuse
    de diffraction par une fente.
\end{manip}

La diffraction
est un phénomène déjà connu
par les étudiants de ce niveau
mais
seulement
sur le plan expérimental.
Il s'agit
d'un phénomène
qui se manifeste
pour tous les types d'ondes
lorsqu'elles rencontrent
un obstacle
son observation nécessite toutefois
que la longuer d'onde de l'onde
ait une taille porche
de la taille de l'objet.

Dans cette leçon
nous cherchons
à modéliser
la propagation de la lumière
pour rendre compte
des observations.

\section{Le modèle ondulatoire de la lumière}

    \subsection{Grandeurs utiles}

    On décrit la lumière
    principalement
    avec
    deux grandeurs~:
    l'amplitude complece du champ électrique $\z{E}(M, t)$
    et l'intensité lumineuse $I(M) = \avg{E(M, t)}^2$
    mesurée par les capteurs.

    La propagation
    de la lumière
    est modélisée par
    la propagation
    de $E$
    comme une onde
    de sorte que~:
    si l'on considère
    une source
    ponctuelle
    monochromatique
    d'amplitude $\z{E_0}$
    en $M$,
    et
    si l'on note $\z{E}(P)$
    l'amplitude au point $P$ à $t$,
    alors,
    au point $P$ hors de la source~:
    \begin{equation*}
        \z{E}(P)
        = \z{E_0} \frac{e^{i \vb{k} \vb{r}}}{r}
    \end{equation*}
    où
    $\vb{r} = \va{MP}$
    et
    $\vb{k}$ est le vecteur d'onde (sphérique)~;
    l'exponentielle complexe
    tiend compte du déphasage
    lié à la propagation de $M$ à $P$.

    Si un point $P$
    est éclairé
    par plusieurs sources ponctuelles
    de même longueur d'onde
    l'amplitude $\z{E}(P)$
    en $P$
    s'écrit~:
    \begin{equation*}
        \z{E}(P)
        = \sum_{i \in \textrm{sources}} \z{E_i} \frac{e^{i\vb{k}\vb{r_i}}}{r_i}
    \end{equation*}

    \subsection{Principe de \propername{Huygens-Fresnel} (1678 \-- 1818)
        \cite{perezoptique}}

    \subsubsection{Énnoncé}

    La lumière
    se propage de proche en proche.
    Dans l'espace,
    chaque élément de surface
    éclairé
    se comporte comme une source secondaire
    qui émet des ondelettes sphériques
    dont l'amplitude
    est proportionnelle à la surface
    et à l'amplitude de l'onde reçue.

    De plus,
    la phase de l'onde émise
    est prise égale
    à la phase de l'onde reçue.
    \begin{draft}
        Attention,
        normalement il faudrait prendre
        un déphasage de $\tfrac{\pi}{2}$
        mais
        il n'aura pas d'importance
        dans ce que nous faisons
        car au final
        on s'intéresse
        à l'amplitude.
    \end{draft}

    \subsubsection{Mathématiquement}

    \begin{figure}[H]
        \centering
        \tdplotsetmaincoords{20}{0}
        \tdplotsetrotatedcoords{0}{110}{0}
        \begin{tikzpicture}[tdplot_rotated_coords]
            \draw[dotted] (-2,0,0) -- (2,0,0);
            \draw[dotted] (0,0,0) -- (0,2,0);
            \draw[dotted] (0,0,-1) -- (0,0,11);
            \draw[thick,->] (2,0,0) -- (2.5,0,0) node[anchor=north east]{$x$};
            \draw[thick,->] (0,2,0) -- (0,2.5,0) node[anchor=south]{$y$};
            \draw[thick,->] (0,0,11) -- (0,0,11.5) node[anchor=west]{$z$};

            \coordinate (O) at (0,0,0);
            \coordinate (M) at (1,1,0);
            \coordinate (Mx) at (0,1,0);
            \coordinate (My) at (1,0,0);
            \coordinate (P) at (2,2,10);
            \coordinate (Px) at (0,2,10);
            \coordinate (Py) at (2,0,10);
            \coordinate (Pxy) at (0,0,10);

            % Dessiner les points
            \draw (O) node[anchor=north]{$O$};
            \draw (M) node[anchor=south east]{$M$};
            \draw[dotted] (Mx) -- (M);
            \draw[dotted] (My) -- (M);
            \draw (P) node[anchor=north west]{$P$};
            \draw[dotted] (Px) -- node{$X$} (P);
            \draw[dotted] (Py) -- node{$Y$} (P);
            \draw[dotted] (Px) -- (Pxy);
            \draw[dotted] (Py) -- (Pxy);

            % Dessiner les vecteurs
            \draw[dashed,->] (O) -- node[anchor=north east]{$\rho$} (M);
            \draw[->] (M) -- node[above]{$\vb{r}$} (P);
            \draw[dashed] (O) -- node[below]{$\vb{R} \parallel \vu{d}$} (P);

            \draw (O) circle (2);
        \end{tikzpicture}
    \end{figure}
    Si l'on considère un diaphragme $D$
    éclairé par une source pontuelle
    alors,
    l'amplitude $\z{E}(P)$
    en un point $P$
    de l'autre côté du diaphragme
    est~:
    \begin{equation*}
        \z{E}(P)
        \propto \iint_{M \in D} \z{E}(M) \frac{e^{i \vb{k}\vb{r}}}{r} \dd S
    \end{equation*}

    Nous n'avons ici
    semble-t-il
    rien gagné puisque
    nous devons maintenant déterminer
    les amplitudes $\z{E}(M)$.
    Mais dans certaines conditions expérimentales
    nous pourrons en fait dire
    que $\z{E}(M) = \z{E_0} \frac{e^{i\vb{k}\vb{r}}}{r} \approx \z{E_M}$
    (diaphragme éclairé uniformément
    par une lumière cohérente,
    donc une source à l'infini).

    L'écriture
    se généralise simplement
    dans les situations
    où plutôt qu'un diaphragme
    nous avons un objet
    de transmittance variable $t(M)$~:
    \begin{equation*}
        \z{E}(P)
        \propto \iint t(M) \frac{e^{i \vb{k}\vb{r}}}{r}\dd S
    \end{equation*}

    \begin{draft}
        En principe,
        on doit choisir
        une surface fermée
        qui englobe la source
        pour appliquer
        le principe.
        Mais avec un diaphragme,
        l'intégrale se résumerait
        à l'ouverture.
    \end{draft}
    
\section{Vers les conditions de \propername{Fraunhofer}}

    Le calcul de l'intégrale
    est compliqué dans le cas général.
    L'approximation de \propername{Fraunhofer}
    consiste à se placer
    dans des conditions expérimentales particulières
    qui simplifient l'expression.
    Nous allons
    les rechercher
    pas à pas.

    \subsection{Le dénominateur}

    Au dénominateur
    nous avons $r$,
    la distance entre
    le point d'intégration
    et le point d'observation.
    L'expression de $\z{E}$
    serait bien simplifiée
    si $r$ était constant
    ou du moins,
    si l'on pouvait négliger
    ses variations
    dans le domaine d'intégration.
    Expérimentalement,
    à quoi est-ce-que cela correspond~?
    La distance $r$
    varie peu
    si la distance $R$
    est très grande
    devant la taille de l'objet,
    en termes mathématiques~:
    \begin{equation*}
        r
        = \sqrt{R^2 + \rho^2 - 2\vb{\rho}\vb{R}}
        = R \sqrt{1
            + \qty(\frac{\rho}{R})^2
            - 2 \qty(\frac{\vb{\rho} \dotproduct \vu{d}}{R})}
        \approx R
        \qq{si}
        \frac{\rho}{R} \ll 1
    \end{equation*}

    Plaçons nous
    dans ces conditions,
    et rééxprimons
    l'amplitude lumineuse en $P$~:
    \begin{equation*}
        \z{E}(P)
        \propto \iint t(M) \frac{e^{i \vb{k} \vb{r}}}{R} \dd S
        \propto \iint t(M) e^{i \vb{k} \vb{r}} \dd S
    \end{equation*}

    \subsection{Le terme de phase}

    Le terme de phase
    $e^{i\vb{k}\vb{r}}$
    va pouvoir s'exprimer
    $e^{i\frac{2\pi}{\lambda}\vu{d}\vb{r}}$
    puis
    $e^{i\frac{2\pi}{\lambda}r}$
    dans la condition
    $\frac{\rho}{R} \ll 1$
    puisque
    $\vb{R}$ et $\vb{r}$
    sont alors
    presque parallèles.

    La phase
    dépend du rapport
    $\frac{r}{\lambda}$.
    Si l'on décide
    de faire un développement limité de $r$
    comme précédement,
    nous devrons faire attention
    à l'importance du rapport
    $\frac{1}{\lambda}\frac{\textrm{terme}}{R}$
    qui diffère
    de l'importance du rapport
    $\frac{\textrm{terme}}{R}$.
    Calculons~:
    \begin{gather*}
        r
        = R \sqrt{1
            + \qty(\frac{\rho}{R})^2
            - 2 \qty(\frac{\vb{\rho} \dotproduct \vu{d}}{R})}
        \approx R \qty[1
            + \frac{1}{2} \qty(\frac{\rho}{R})^2
            - \qty(\frac{\vb{\rho} \dotproduct \vu{d}}{R})]
        \\
        \implies
        \vb{k}\vb{r}
        = \frac{2\pi}{\lambda} r
        \approx \lambda R
            + \frac{\rho^2}{2\lambda R}
            - \frac{\vb{\rho}\dotproduct\vu{d}}{\lambda}
    \end{gather*}
    Dans l'approximation de \propername{Fraunhofer},
    on cherche à négliger
    le terme en $\rho^2$
    ce qui nécessite la condition~:
    $\flatfrac{\rho^2}{\lambda R} \ll 1$.
    Dans ce cas,
    $\z{E}(P)$
    devient~:
    \begin{equation*}
        \z{E}(P)
        \propto \iint t(M) e^{i k R} e^{- i k \vb{\rho}\vu{d}} \dd S
        \propto \iint t(M) e^{- i k \vb{\rho}\vu{d}} \dd S
    \end{equation*}

    \subsection{Résultat}

    Enfin,
    on peut développer
    le produit scalaire
    $\vb{\rho}\vu{d}$
    que nous traînons
    pour exprimer l'amplitude
    en fonctions des coordonnées
    $M(x, y)$ et $P(X, Y)$~:
    \begin{equation*}
        \z{E}(X, Y)
        \propto \iint t(x, y) e^{-i \frac{2\pi}{\lambda R} (xX+yY)} \dd x \dd y
    \end{equation*}

    et l'on reconnaît ici
    la transformée de \propername{Fourier}
    de $t(x, y)$
    par rapport
    à ses deux variables.
    Sur l'écran
    placé de sorte que
    $\flatfrac{\rho^2}{\lambda R} \ll 1$
    et
    $\flatfrac{\rho}{R} \ll 1$,
    on pourra observer
    une image
    qui se trouve être
    la transformée de \propername{Fourier}
    de l'image géométrique
    de l'objet diffractant.

    \paragraph{Ordres de grandeurs}
    \begin{todo}
        Montrer qu'il faut
        les deux conditions
        et que l'une
        n'implique pas l'autre.
    \end{todo}

\section{Exemples de figures de diffraction}

    \begin{todo}
        Toutes les solutions
        ne sont pas analytiques,
        montrer le résultat
        pour la fente
        puis
        une simulation
        pour quelque chose
        de plus amusant.
    \end{todo}

\section{Optique de \propername{Fourier}}

    \begin{todo}
        Avec les transformées de \propername{Fourier}
    \end{todo}
