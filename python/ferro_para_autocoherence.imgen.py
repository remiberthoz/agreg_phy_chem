#!/usr/env python3
# -*- coding: utf-8 -*-
import imgen
imgen.setup()

import matplotlib.pyplot as plt
import numpy as np

X = np.linspace(-25, 25, 8000)
B = 0
Tc = 1

Tplot = [0.5*Tc, 2*Tc]  # Valeurs à tracer explicitement
Tspace = np.linspace(0.1, 1.5*Tc, 200)

M1 = np.tanh(X)  # Aimantation 1

###############################################
# Solve Msp
def solve_f_eq_g(f, g):
    """ Retroune le premier indice pour lequel f = g,
        en partant de la fin de l'axe
    """
    fbig = f[-1] > g[-1]
    for k in range(len(f)-1, -1, -1):
        if fbig:
            if f[k] < g[k]:
                return k
        else:
            if f[k] > g[k]:
                return k

Msp = []  # Liste des aimantations spontannées à T in Tspace
Xsp = []
for T in Tspace:
    M2 = X * T/Tc - B  # Aimantaiton 2
    k = solve_f_eq_g(M1, M2)  # On cherche quand Aim1 = Aim2
    Msp.append(M1[k])
    Xsp.append(X[k])

###############################################
# Plot
fig, ax = plt.subplots(1, 2, sharey=True)
fig.suptitle(r'$B = 0$')

ax_M = ax[1]
ax_M.plot(Tspace, Msp, color='k')
ax_M.set_xlim(0, Tspace[-1])
ax_M.set_xlabel(r'$T/T_c$')

ax_autoco = ax[0]
ax_autoco.plot(X, M1, color='k', label='tanh')
ax_autoco.set_xlim(-2.5, 2.5)
ax_autoco.set_xlabel(r'$x$')
ax_autoco.set_ylabel(r'$M / M_s$')
ax_autoco.set_ylim((-1.1, 1.1))

Msp = []  # Liste des aimantations spontannées à T in Tspace
Xsp = []
for n,T in enumerate(Tplot):
    # Calculs
    M2 = X * T/Tc - B
    k = solve_f_eq_g(M1, M2)
    # Plot de la droite
    ax_autoco.plot(X, M2, label=r'$T_%d$' % n)
    col = ax_autoco.get_lines()[-1].get_color()
    # Plot du point associé
    ax_M.plot(T, M1[k], linestyle='None', marker='o', color=col)
    # Lignes horizontales
    ax_autoco.plot((X[k], X[-1]), (M1[k], M1[k]), color=col, linestyle='dotted')
    ax_M.plot((0, T), (M1[k], M1[k]), color=col, linestyle='dotted')
# Plot Tc
ax_autoco.plot(X, X * Tc/Tc - B, label=r'$T_c$', color='k', linestyle='dashed')
ax_M.axvline(Tc, linestyle='dashed', color='k')
ax_autoco.legend()

plt.subplots_adjust(wspace=0.03, hspace=0.03)
# Save
imgen.done(__file__)
