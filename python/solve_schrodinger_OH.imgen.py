#!/usr/env python
# -*- coding: utf-8 -*-
import imgen
imgen.setup()

import numpy as np
# Paramètres du problème
mm = 1
omega0 = 2 * np.pi
x0 = 0
args = {'omega0': omega0, 'mm': mm, 'x0': x0}

x_min = -np.pi
x_max = +np.pi
N = 800

# Potentiel
def OH(X, args):
    omega0 = args['omega0']
    mm = args['mm']
    x0 = args['x0']

    U = 1/2.0 * mm * (omega0 ** 2) * ((X - x0) ** 2)

    return U

import solve_schrodinger
# Résolution
E, Y = solve_schrodinger.solve(x_min, x_max, N, mm, OH, args)
NN = 8
E = E[:NN]
Y = Y[:NN]

import matplotlib.pyplot as plt
# Affichage
solve_schrodinger.plot(x_min, x_max, N, E, Y, OH, args)

# Save
imgen.done(__file__)
