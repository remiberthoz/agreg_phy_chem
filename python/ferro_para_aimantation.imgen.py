#!/usr/env python3
# -*- coding: utf-8 -*-
import imgen
imgen.setup()

import matplotlib.pyplot as plt
import numpy as np

X = np.linspace(0, 4, 200)

plt.plot(X, np.tanh(X), label="Quantique")
plt.plot(X, 1 / np.tanh(X) - 1/X, label="Classique", linestyle="dashed")

plt.xlabel(r'$x$')
plt.ylabel(r'$M(x) / M_s$')
plt.legend()

# Save
imgen.done(__file__)
