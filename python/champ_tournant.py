#!/usr/env python3
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.animation import FuncAnimation

angle = np.linspace(-np.pi, np.pi, 20, endpoint=False)
B1 = np.cos(angle)
B2 = np.cos(angle - np.pi/2)

ax0 = plt.subplot(121, projection='polar')
ax0.set_xticks([])
ax0.set_yticks([0])
ax0.set_yticklabels([''])
ax0.set_xlim(-np.pi/2, 3*np.pi/2)
ax0.set_ylim(-1, 1)

ax1 = plt.subplot(122, projection='polar')
ax1.set_xticks([])
ax1.set_yticks([0])
ax1.set_yticklabels([''])
ax1.set_xlim(-np.pi/2, 3*np.pi/2)
ax1.set_ylim(-1, 1)

zeros = np.zeros_like(angle)
l_B1 = ax0.quiver(angle, zeros, B1*np.cos(angle), B1*np.sin(angle), units='xy', scale_units='xy', scale=1, color='blue')
l_B2 = ax0.quiver(angle, zeros, B2*np.cos(angle), B2*np.sin(angle), units='xy', scale_units='xy', scale=1, color='red')
B = B1 + B2
l_B = ax1.quiver(angle, zeros, B*np.cos(angle), B*np.sin(angle))
iBmax = np.argmax(B)
l_Bmax, = ax1.plot(angle[iBmax], B[iBmax], marker='o')

omega = 1
dt = 0.01
def animate(i):
    t = i*dt
    B1t = B1 * np.cos(omega*t)
    B2t = B2 * np.cos(omega*t - np.pi/2)
    Bt = 2 * (B1t + B2t)
    iBmax = np.argmax(Bt)
    l_B1.set_UVC(B1t*np.cos(angle), B1t*np.sin(angle))
    l_B2.set_UVC(B2t*np.cos(angle), B2t*np.sin(angle))
    l_B.set_UVC(Bt*np.cos(angle), Bt*np.sin(angle))
    l_Bmax.set_data(angle[iBmax], Bt[iBmax])

anim = FuncAnimation(plt.gcf(), animate, interval=1)

plt.show()
