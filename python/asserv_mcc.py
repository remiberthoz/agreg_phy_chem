#!/usr/env python3
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def modPasseBas(f, A0, f0, Q):
    return np.abs(A0 / (1 - (f/f0)**2 + 1j*f/f0/Q))

def argPasseBas(f, A0, f0, Q):
    return np.angle(A0 / (1 - (f/f0)**2 + 1j*f/f0/Q))

##############################################################################
##############################################################################
# Linéarité de la chaine de retour
##############################################################################
##############################################################################
"""
##############################################################################
# Linéarité cmd/vit en commande continue
data = np.asarray([ #[cmd,vit]
        [4,363.629],
        [3,272.722],
        [2,181.815],
        [1,90.907]
    ])

plt.plot(data[:,0], data[:,1], marker='+', linestyle='dotted')
plt.show()
"""
"""
##############################################################################
##############################################################################
# Diagramme de Bode
##############################################################################
##############################################################################
fig, axes = plt.subplots(2, sharex=True)
axG = axes[0]
axP = axes[1]

axP.set_xlabel(r'$f$')
axP.set_ylabel(r'$\arg(H)$')
axG.set_ylabel(r'$20.\log(|H|)$')

f_space = np.logspace(0, 4, 100)
##############################################################################
# Diagramme de bode FTBO, moteur seul
data = np.asarray([ #[freq,e,r]
        [0,999.99],
        [2,999.586],
        [3,999.081],
        [5,997.47],
        [10,989.985],
        [20,961.134],
        [30,916.615],
        [40,860.794],
        [50,798.289],
        [60,733.217],
        [70,669.045],
        [80,607.224],
        [90,550.91],
        [100,497.344]
    ])

data[:,1] = data[:,1]*1e-3/1

axG.semilogx(data[:,0], 20*np.log10(data[:,1]), marker='+', linestyle='None')
col = axG.get_lines()[-1].get_color()  # Récupère la couleur

# Fit de la courbe
popt, pcov = curve_fit(modPasseBas, data[:,0], data[:,1], bounds=[(0), (np.inf)])
axG.plot(f_space, 20*np.log10(modPasseBas(f_space, *popt)), label='A0 = {:2.2f}, f0 = {:2.2f}, Q = {:2.2f}'.format(*popt), color=col, marker='None')
axG.axvline(-np.pi, color=col, linestyle='dotted')

axP.semilogx(f_space, argPasseBas(f_space, *popt), color=col, marker='None')
axP.axvline(-np.pi, color=col, linestyle='dotted')

axP.axhline(-np.pi, color='k', linestyle='dotted')

##############################################################################
# Diagramme de bode FTBO, correcteur P, K=0.5
data = np.asarray([ #[freq,e,r]
        [  1,666.720,331.311],
        [ 10,671.883,332.565],
        [ 20,687.224,330.246],
        [ 30,711.779,326.202],
        [ 40,744.031,320.218],
        [ 50,781.919,312.088],
        [ 60,822.986,301.703],
        [ 70,864.633,289.119],
        [ 80,904.545,274.593],
        [ 90,940.530,258.570],
        [100,971.634,241.61],
    ])

data[:,1] = data[:,2]/data[:,1]

axG.semilogx(data[:,0], 20*np.log10(data[:,1]), marker='+', linestyle='None')
col = axG.get_lines()[-1].get_color()  # Récupère la couleur

# Fit de la courbe
popt, pcov = curve_fit(modPasseBas, data[:,0], data[:,1], bounds=[(0), (np.inf)])
axG.plot(f_space, 20*np.log10(modPasseBas(f_space, *popt)), label='A0 = {:2.2f}, f0 = {:2.2f}, Q = {:2.2f}'.format(*popt), color=col, marker='None')
axG.axvline(-np.pi, color=col, linestyle='dotted')

axP.semilogx(f_space, argPasseBas(f_space, *popt), color=col, marker='None')
axP.axvline(-np.pi, color=col, linestyle='dotted')

##############################################################################
# Diagramme de bode FTBO, correcteur P, K=5
data = np.asarray([ #[freq,e,r]
        [  1,166.697,833.327],
        [  2,166.753,833.355],
        [  3,166.848,833.402],
        [ 10,168.522,834.254],
        [ 20,174.197,837.068],
        [ 30,183.684,841.711],
        [ 40,197.133,848.387],
        [ 50,214.412,856.942],
        [ 60,236.638,867.464],
        [ 70,263.177,879.979],
        [ 80,294.643,894.500],
        [ 90,331.392,911.019],
        [100,373.809,929.484],
    ])

data[:,1] = data[:,2]/data[:,1]

axG.semilogx(data[:,0], 20*np.log10(data[:,1]), marker='+', linestyle='None')
col = axG.get_lines()[-1].get_color()  # Récupère la couleur

# Fit de la courbe
popt, pcov = curve_fit(modPasseBas, data[:,0], data[:,1], bounds=[(0), (np.inf)])
axG.plot(f_space, 20*np.log10(modPasseBas(f_space, *popt)), label='A0 = {:2.2f}, f0 = {:2.2f}, Q = {:2.2f}'.format(*popt), color=col)
axG.axvline(-np.pi, color=col, linestyle='dotted')

axP.semilogx(f_space, argPasseBas(f_space, *popt), color=col)
axP.axvline(-np.pi, color=col, linestyle='dotted')

##############################################################################
# Diagramme de bode FTBO, correcteur I, tau=100
data = np.asarray([ #[freq,e,r]
        [  1,062.795,999.280],
        [1.5,094.121,998.393],
        [  2,125.362,997.153],
        [  3,187.471,993.622],
        [ 10,591.805,993.622],
        [ 20,1003,767.212],
        [ 30,1196,581.705],
        [ 40,1247,427.071],
        [ 50,1277,313.542],
    ])

data[:,1] = data[:,2]/data[:,1]

axG.semilogx(data[:,0], 20*np.log10(data[:,1]), marker='+', linestyle='None')
col = axG.get_lines()[-1].get_color()  # Récupère la couleur

# Fit de la courbe
popt, pcov = curve_fit(modPasseBas, data[:,0], data[:,1], bounds=[(0), (np.inf)])
axG.plot(f_space, 20*np.log10(modPasseBas(f_space, *popt)), label='A0 = {:2.2f}, f0 = {:2.2f}, Q = {:2.2f}'.format(*popt), color=col)
axG.axvline(-np.pi, color=col, linestyle='dotted')

axP.semilogx(f_space, argPasseBas(f_space, *popt), color=col)
axP.axvline(-np.pi, color=col, linestyle='dotted')

##############################################################################
# Diagramme de bode FTBO, correcteur I, tau=2000 (C > Ccrit)
# on ne peut pas vraiment le tracer...

axG.legend()
"""
##############################################################################
##############################################################################
# Erreur et temps carac
##############################################################################
##############################################################################

##############################################################################
# Correction prop: Erreur = f(K)
data = np.asarray([ #[K, r]
        [10,909],
        [5,833.318],
        [4,799],
        [3,749],
        [2,666],
        [1,499],
        [0.5,333],
    ])
data[:,1] = 1-data[:,1]*1e-3
plt.plot(data[:,0], data[:,1])

# Correction prop: Deltat t = f(K)
data = np.asarray([ #[K, dt]
        [10,5.1],
        [5,8.0],
        [4,7.4],
        [3,7.6],
        [2,9.2],
        [1,7.9],
        [0.5,8.41],
    ])
plt.plot(data[:,0], data[:,1])

##############################################################################
##############################################################################
##############################################################################
##############################################################################
plt.show()
