#!/usr/env python
# -*- coding: utf-8 -*-
import imgen
imgen.setup()

import numpy as np
# Paramètres du problème
v0 = 1
a = 1
d = .1
mm = 1
args = {'a': 1, 'd': d, 'v0': v0, 'mm': mm}

x_max = a + d/2 + 1
x_min = -x_max
N = 700

# Potentiel
def NH3(X, args):
    a = args['a']
    d = args['d']
    v0 = args['v0']

    U = np.zeros_like(X)
    for n in range(len(X)):
        x = X[n]
        if x < - (a+d/2) or x > (a+d/2):
            U[n] = 100
        elif x < -d/2 or x > d/2:
            U[n] = 0
        else:
            U[n] = v0

    return U

import solve_schrodinger
# Résolution
E, Y = solve_schrodinger.solve(x_min, x_max, N, mm, NH3, args)
NN = 7
E = E[:NN]
Y = Y[:NN]

import matplotlib.pyplot as plt
# Affichage
fig, ax = plt.subplots(3, 1, sharex=True)
solve_schrodinger.plot(x_min, x_max, N, E, Y, NH3, args, ax[0])
solve_schrodinger.plot(x_min, x_max, N, E[:2], Y[:2], NH3, args, ax[1])
ax[1].set_ylim(0.075, 0.140)

# Gauche et droite
YG = Y[0] + Y[1]
YD = Y[0] - Y[1]
E = E[:2]
Y = np.array([YG, YD])
solve_schrodinger.plot(x_min, x_max, N, E, Y, NH3, args, ax[2])
ax[2].set_ylim(0.075, 0.140)

# Save
imgen.done(__file__)
