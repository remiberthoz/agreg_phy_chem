#!/usr/env python3
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.widgets import Slider, Button
from matplotlib.animation import FuncAnimation

ax = plt.gca()

H = 1
L = 2
xlim = (-10*L, 10*L)
ylim = (-0.1, H+1)

ax.set_ylim(*ylim)
ax.set_yticks([])
ax.set_xlim(*xlim)

box = mpl.patches.Rectangle([0, 0], L, H, fill=True, color='grey')
plate = mpl.patches.Rectangle([xlim[0], -0.1], (xlim[1]-xlim[0]), 0.1, fill=True, color='k')
forceF = plt.quiver(L/2, H/2, 0, 0, units='xy', scale_units='xy', scale=1, color='blue')
forceFf = plt.quiver(L/2, H/2, 0, 0, units='xy', scale_units='xy', scale=1, color='black')
forceFres = plt.quiver(L/2, H/2, 0, 0, units='xy', scale_units='xy', scale=1, color='red')
forceFlim, = plt.plot([], [], color='k', linestyle='None', marker='+')
box.set_zorder(0)
forceF.set_zorder(10)
forceFf.set_zorder(10)
forceFres.set_zorder(10)
ax.add_patch(box)
ax.add_patch(plate)


# ############################################################################
# Mise à jour avec sliders
mu_stat = 0.6
mu_dyn = 0.4
m = 1
g = 10
F = 0
v = 0
x = 0

def update_params(_):
    global F;
    F = slider_F.val

# Slider J
ax_F = plt.axes([0.1, 0.10, 0.6, 0.03])
slider_F = Slider(ax_F, 'F', 0, 10, F)
slider_F.on_changed(update_params)


# ############################################################################
# Simulation
dtanim = 25  # ms
dt = dtanim*1e-3  # s
def animate(i):
    # Get updated vars
    global F, v, x
    # Compute stuff
    Ff = 0
    Fstat = - mu_stat * m * g
    Fdyn = - mu_dyn * m * g
    if v < 1e-1:
        if F < np.abs(Fstat):
            v = 0
            Ff = -F
        else:
            Ff = Fdyn
    else:
        Ff = Fdyn
    Fres = F + Ff
    v = v + Fres*m*dt
    x = x + v*dt

    if x > ax.get_xlim()[1]:
        x = ax.get_xlim()[0]
    # Move stuff
    box.set_xy([x, 0])

    X = x+L/2
    Y = 3*H/4
    Yres = H/2

    forceF.set_offsets([X, Y])
    forceF.set_UVC(F, 0)

    forceFf.set_offsets([X, Y])
    forceFf.set_UVC(Ff, 0)

    forceFres.set_offsets([X, Yres])
    forceFres.set_UVC(Fres, 0)

    forceFlim.set_data([X+Fstat, Y])

anim = FuncAnimation(plt.gcf(), animate, interval=dtanim)

# Output
plt.gcf().subplots_adjust(bottom=0.2)  # Fait de la place pour les sliders
plt.show()
