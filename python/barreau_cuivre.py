#!/usr/env python3
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize as spo

# https://physique-pt-cluny.monsite-orange.fr/file/a016cc98ec2bb13e2494ea50256faf64.pdf

################################################################################

Text = 23  # °C, température extérieure
a = 12e-2  # m, pas de mesure de la température
k = 1  # mV/°C ?, gain des capteurs de température
L = 0.846  #m, longueur de la barre
R = 0.011  # m, rayon de la barre

Palim = 20  # W, puissance du bloc chauffant

rho = 8.9e3  # kg/m3, masse volumique
c = 385  # J/kg/K, capa calo

################################################################################
# data = [Instant de mesure (s), *[Valeurs relevées (°C)]]

mesures = np.asarray([
        [100, 23.8,23.1,23.0,23.0,23.0],
        [200, 28.1,24.2,23.2,23.0,23.0],
        [300, 33.8,26.9,24.2,23.2,23.1],
        [400, 37.8,29.9,25.8,24.0,23.3],
        [500, 40.7,32.5,27.5,24.9,23.8],
        [600, 42.9,34.6,29.3,25.9,24.4],
        [700, 44.7,36.5,30.8,27.1,25.1],
        [800, 46.0,38.1,32.2,28.3,25.9],
        [900 ,47.1,39.3,33.5,29.4,26.7],
        [1000,48.1,40.5,34.6,30.3,27.5],
        [1100,48.8,41.5,35.6,31.3,28.4],
        [1200,49.5,42.4,36.5,32.2,29.2],
        [1300,50.2,43.1,37.4,33.1,29.9],
        [1400,50.8,43.9,38.3,33.9,30.5],
        [1500,51.1,44.5,38.9,34.5,31.1],
        [10000,56.4,51.6,47.3,43.2,39.1]
    ])
mesures_offset = np.asarray([
        [0000,   0,   0,   0,   0,   0]
    ])  # utile si les capteurs donnent des valeurs différentes au départ
   
mesures[:, 1:] = (mesures[:, 1:] - mesures_offset[:, 1:]) * k

Vx = a * np.arange(len(mesures[0]) - 1)  # Positions des mesures
Vt = mesures[:, 0]  # Instant des mesures 
VT = mesures[:, 1:]  # Températures mesurées

################################################################################
################################################################################
################################################################################
# Plot Tx = f(t)
plt.figure(0)
plt.xlabel(r'$t$ / s')
plt.ylabel(r'$T$ / °C')
plt.title(r'Température en fonction du temps')
for xx in range(len(Vx)):
    plt.plot(Vt, VT[:, xx], label='$x={}$m'.format(Vx[xx]))

plt.legend()

# Plot Tt = f(x)
plt.figure(1)
plt.xlabel('$x$ / m')
plt.ylabel('$T$ / °C')
plt.title(r'Température en fonction de la position')
for tt in range(len(Vt)):
    plt.plot(Vx, VT[tt], label='$t={}$s'.format(Vt[tt]))

plt.legend()

################################################################################
# Dérivées (seulement celles qui sont utiles)

# Calcul des dérivées
dt = []
d2x = []
for xx in range(1, len(Vx) - 1):
    for tt in range(1, len(Vt) - 1):
        dt.append((VT[tt+1, xx] - VT[tt-1, xx]) / (Vt[tt+1] - Vt[tt-1]))
        d2x.append((VT[tt, xx+1] - 2*VT[tt, xx] + VT[tt, xx-1]) / ((Vx[xx+1] - Vx[xx-1])/2)**2)

dt = np.asarray(dt)
d2x = np.asarray(d2x)

# Plot de la relation de diffusion (d2x = D * dt)
plt.figure(2)
plt.xlabel(r'$\frac{d^2 T}{d x^2}$')
plt.ylabel(r'$\frac{d T}{d t}$')
plt.title(r'Équation de diffusion: $\frac{d T}{d t} = D \cdot \frac{d^2 T}{d x^2}$')
plt.plot(dt, d2x, marker='+', linestyle='None')

################################################################################
# Calcul de la valeur moyenne de lambda
D = dt / d2x
D = D[np.logical_not(np.isnan(D))]
D = D[np.logical_not(np.isinf(D))]

l = rho*c*D

avg = np.mean(l)
std = np.std(l)

print('Conductivité thermique lambda')
print(' Moyenne de lambda: {} W/m/K'.format(avg))
print(' Écart type sur lambda: {} W/m/K'.format(std))

l = 389  # W/m/K
print(' Valeur tab de lambda: {} W/m/K'.format(l))

print()
################################################################################
################################################################################
################################################################################
# Loi de Fourier, régime permanent
VTperm = VT[-1]
plt.figure(3)
plt.xlabel(r'$x$ / m')
plt.ylabel(r'$T$ / °C')
plt.title(r'Température en fonction de la position, en régime permanent')
plt.plot(Vx, VTperm, marker='+', linestyle='None', label=r'Exp.')

# Fit
def Fourier(x, A, B):
    return A*x + B

popt, pcov = spo.curve_fit(Fourier, Vx, VTperm)
Vxfit = np.linspace(Vx[0], Vx[-1], 100)
plt.plot(Vxfit, Fourier(Vxfit, *popt), label=r'Modèle')

phi = - popt[0] * np.pi * R**2
print('Régime permanent : flux thermique Phi')
print(' Phi = {} W'.format(phi))
print(' Puissance alim = {} W'.format(Palim))

print()
################################################################################
################################################################################
################################################################################
# Loi de Fourier, régime permanent, avec pertes
plt.figure(4)
plt.xlabel(r'$x$ / m')
plt.ylabel(r'$T$ / °C')
plt.title(r'Température en fonction de la position, en régime permanent')
plt.plot(Vx, VTperm, marker='+', linestyle='None', label=r'Exp.')

# Fit
def Fourier(x, A, B, C):
    return A*np.exp(-B*x) + C

popt, pcov = spo.curve_fit(Fourier, Vx, VTperm, bounds=[0, np.inf])
Vxfit = np.linspace(Vx[0], Vx[-1], 100)
plt.plot(Vxfit, Fourier(Vxfit, *popt), label=r'Modèle')

h = popt[1]**2 * l * R / 2
print('Régime permanent : pertes par h')
print(' h = {} W/m2/K'.format(h))

################################################################################
################################################################################
################################################################################
# Affichage
plt.show()
