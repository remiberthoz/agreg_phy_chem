#!/usr/env python3
# -*- coding: utf-8 -*-
import imgen
args = imgen.setup()
savefig = args.save

from dependencies.wavelen2rgb import wavelen2rgb
import colorsys
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import numpy as np

L = [435.7e-9, 546.1e-9]

RGB = [np.asarray(wavelen2rgb(l*1e9, 255)) / 255. for l in L]
HSV = [colorsys.rgb_to_hsv(rgb[0], rgb[1], rgb[2]) for rgb in RGB]
HUE = [hsv[0] for hsv in HSV]
SAT = [hsv[1] for hsv in HSV]

theta = np.linspace(-np.pi/4, +np.pi/4, 1e5)

def calc_I(theta, n, e, N, l):
    k = 2*np.pi/l  # nombre d'onde
    alpha = np.sin(theta) * k/(n*1e3)/2
    beta = e*n*1e3

    if N != 0:
        s = np.sin(beta*alpha)/(beta*alpha) * (np.sin(N*alpha)/np.sin(alpha))
    else:
        s = np.sin(beta*alpha)/(beta*alpha)
    I = s**2 / (np.max(s**2))

    return I

def plot_sp(axes, n, e, N):
    axes[0].set_xlim(-np.pi/4, +np.pi/4)
    axes[0].set_xticks([-np.pi/4, 0, np.pi/4])
    axes[0].set_xticklabels([r'$-\frac{\pi}{4}$','0',r'$+\frac{\pi}{4}$'])

    I1 = calc_I(theta, n, e, N, L[0])
    I2 = calc_I(theta, n, e, N, L[1])

    line1, = axes[0].plot(theta, I1)
    line2, = axes[0].plot(theta, I2)

    COL1 = np.zeros((10, len(theta), 3))
    COL2 = np.zeros((10, len(theta), 3))
    COL3 = np.zeros((10, len(theta), 3))
    for index in range(len(theta)):
        COL1[:, index] = colorsys.hsv_to_rgb(HUE[0], SAT[0], I1[index])
        COL2[:, index] = colorsys.hsv_to_rgb(HUE[1], SAT[1], I2[index])
        COL3[:, index] = (COL1[0, index] + COL2[0, index])
    axes[1].imshow(COL1, aspect='auto', extent=[theta[0], theta[-1], 0, 1])
    axes[2].imshow(COL2, aspect='auto', extent=[theta[0], theta[-1], 0, 1])
    axes[3].imshow(COL3, aspect='auto', extent=[theta[0], theta[-1], 0, 1])

fig, axes = plt.subplots(4, 2, sharex=True, sharey=True)
axes[0, 0].set_ylabel(r'$I / I_0$')
axes[3, 0].set_xlabel(r'$\theta$')
axes[3, 1].set_xlabel(r'$\theta$')

n = 300  # traits/mm
e = 1e-6  # largeur des fentes
N = 3  # nombre de traits éclairés
plot_sp(axes[:, 0], n, e, N)

n = 300  # traits/mm
e = 1e-6  # largeur des fentes
N = 10  # nombre de traits éclairés
plot_sp(axes[:, 1], n, e, N)

# Save
imgen.done(__file__)
