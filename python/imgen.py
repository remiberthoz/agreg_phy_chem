def setup(parser=None):
    # Parse argument, whether or not to save the generated image
    import argparse

    if parser is None:
        parser = argparse.ArgumentParser()
    parser.add_argument('--save', action='store_true')
    global args
    args = parser.parse_args()

    # Prepare pyplot for latex rendering
    import matplotlib.pyplot as plt

    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')

    return args


def done(callingfile):
    # Save image if asked
    import matplotlib.pyplot as plt
    from os.path import realpath, dirname, join, basename
    from re import sub

    global args
    if args.save:
        savedir = join(dirname(dirname(realpath(__file__))), '.build/')
        savename = sub('.imgen.py', '_imgen.pdf', basename(realpath(callingfile)))
        savepath = join(savedir, savename)
        plt.savefig(savepath, bbox_inches='tight')
    else:
        plt.show()
